<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

  <!-- CHIEF CREATIVE 2014
  ================================================== -->
  <meta charset="utf-8">
  <title>Corporate - Equity Life</title>
  <meta name="description" content="Chief Creative Labs">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


  <link rel="stylesheet" href="../stylesheets/grid.css">
  <link rel="stylesheet" href="../stylesheets/media.css">
  <link rel="stylesheet" href="../stylesheets/styles.css">
  <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="../js/Chart.js"></script>

  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="../images/favicon.ico">

</head>
<body>

<style>
.i2{
  width: 210px!important;
  border: solid black 2px!important;
  float: left!important;
  margin-right: 30px!important;
}

.i1{
  width: 450px!important;
  border: solid black 2px!important;
  float: left!important;
}

.i3 {
width: 140px!important;
border: solid black 2px!important;
float: left!important;
margin-right: 15px!important;
}
.data{
  margin-top: 20px;
padding: 32px;
border: solid rgb(167, 167, 167) 3px;
font-size: 15px;
}
.iqscore{
  height: 300px;
background-image: none!important;
background-color: black!important;
background-size: cover;
background-position-y: -35px;
}
</style>

<script>

  $(function(){



    var lineData = {
      labels: ["", "", "", "", "", "", ""],
      datasets: [
          {
              label: "",
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: [645,708,731,744,762]
          },
      ]
    };
    var ctx = document.getElementById("lineChart").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData,{bezierCurve:false});

    var donutData = [
    {
        value: 60,
        color:"#514848",
        highlight: "#bdbdbd",
        label: "Inquires"
    },
    {
        value: 45,
        color: "#585555",
        highlight: "#bdbdbd",
        label: "Average Ages of Accounts"
    },
    {
        value: 20,
        color: "#6f6f6f",
        highlight: "#bdbdbd",
        label: "Age of Credit History"
    },
    {
        value: 65,
        color: "#242323",
        highlight: "#bdbdbd",
        label: "No Inshallment Account"
    },
    {
        value: 180,
        color: "#3e3c3c",
        highlight: "#bdbdbd",
        label: "Age of Credit History"
    }];
    var dctx = document.getElementById("donutChart").getContext("2d");
    var myPieChart = new Chart(dctx).Pie(donutData);






  })

</script>


<style>
.container .nine.columns{
  width: 470px;
}

.column-l{
  width: 20%;
  float: left;
  padding-top:40px;

}

.column-r {
  width: 60%;
  float: left;
  background-color: white;
  padding-left: 15px;
  height: 100%;
  padding-right: 15px;
}

.column-l a {
  color: black;
  font-family: futura;
  text-transform: none;
  font-size: 17px;
  display: block;
}

body{
  background: #E7E7E7;
}

h3{
  color: black;
}
.indice td{
  padding-right: 10px;
  padding-left: 10px;
  text-align: center;
}
</style>

<div class="top-bar">
<div class="container" >

  <a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>


  <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
      <nav>

    <a href="#" id="menu-icon"></a>
        <ul>
        <li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
        <li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>
        <li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
        <li><a href="http://equitylife.org/innovation">Innovation</a></li>
        <li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
        <li><a href="http://equitylife.org/what-we-love">What We Love</a></li>

              </ul>

          </nav>






        <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>




</div>

</div>



<div class="iqscore" style="height: 400px;">
<div class="container" >
  <div class="" style="padding-top: 15%;" align="center">
    <h1>CREDIT REPORT</h1>
  </div>




</div></div>
<div class="container" style="height: 75%;">

  <div class="column-l">

    <a href="../my-alerts">Account Summary</a><br />
    <a href="../credit-report/">Credit Report</a><br />
    <a href="../score-manager/">Score Manager</a><br />
    <a href="../simulator">Simulator</a><br />
    <a href="../my-profile/">Profile</a><br />

  </div>
  <div class="column-r">

<table class="indice" style="margin-top: 50px;font-size: 13px;">
  <tr>
    <td>
Real Estate (0)
    </td>
    <td>Revolving (3)
    </td>
    <td>Installment (1) </td>
    <td>Collections/Chargeoffs (3)</td>
    <td>Uncategorized (0)</td>
    <td>Public Records (0) </td>
    <td>Inquiries (8)</td>
  </tr>
</table>

    <table class="data" width="100%;" style="margin-top: 20px">
                <tbody><tr class="applloyGridAltItem">

                    <td align="center"  style="font-weight: bold;">
                        Account Name
                    </td>
                    <td align="center" style="font-weight: bold;">
                        Account Number
                    </td>
                    <td align="center"  style="font-weight: bold;">
                        Date Opened
                    </td>
                    <td align="center"  style="font-weight: bold;">
                        Status
                    </td>
                    <td align="center"  style="font-weight: bold; text-align: right;">
                        Credit Limit
                    </td>
                    <td align="center" style="font-weight: bold; text-align: right;">
                        Current Balance
                    </td>
                </tr>

            <tr class="AccountTrig">

                <td align="center" >
                    <a onclick="return false;" id="MainContent_laInstallments_rprAccount_LinkButton1_0" href="javascript:__doPostBack('ctl00$MainContent$laInstallments$rprAccount$ctl01$LinkButton1','')">TOYOTA MTR</a>
                </td>
                <td align="center" >
                    <span id="MainContent_laInstallments_rprAccount_Label1_0">7040066458826XXXX</span>
                </td>
                <td align="center" >
                    <span id="MainContent_laInstallments_rprAccount_Label2_0">Dec 1, 2008</span>
                </td>
                <td align="center" >
                    <span id="MainContent_laInstallments_rprAccount_Label3_0">Paid</span>
                </td>
                <td align="center"  style="text-align: right;">
                    <span id="MainContent_laInstallments_rprAccount_Label4_0">$19,655.00</span>
                </td>
                <td align="center"  style="text-align: right;">
                    <span id="MainContent_laInstallments_rprAccount_Label5_0">$0.00</span>
                </td>
            </tr>
            <tr class="AccountTrigContent" style="display: none;">
                <td align="center" bgcolor="#F2F4DD" colspan="7">

<div style="border: 1px solid gray;">
    <table width="100%" border="0" cellpadding="10" cellspacing="1" class="font12px">
        <tbody><tr>
            <td class="detailedTableLabel">
                Account Name
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblAccountName_0">TOYOTA MTR</span>
            </td>
            <td class="detailedTableLabel">
                Credit Limit
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblCreditLimit_0">$19,655.00</span>
            </td>
        </tr>
        <tr>
            <td class="detailedTableAltLabel">
                Account Number
            </td>
            <td class="detailedTableAltValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblAccountNumber_0">7040066458826XXXX</span>
            </td>
            <td class="detailedTableAltLabel">
                Current Balance
            </td>
            <td class="detailedTableAltValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblCurrentBalance_0">$0.00</span>
            </td>
        </tr>
        <tr>
            <td class="detailedTableLabel">
                Date Opened
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblDateOpened_0">12/1/2008</span>
            </td>
            <td class="detailedTableLabel">
                High Balance
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblHighBalance_0"></span>
            </td>
        </tr>
        <tr>
            <td class="detailedTableAltLabel">
                Status
            </td>
            <td class="detailedTableAltValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblStatus_0">Paid</span>
            </td>
            <td class="detailedTableAltLabel">
                Minimum Payment
            </td>
            <td class="detailedTableAltValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblMinPayment_0">$0.00</span>
            </td>
        </tr>
        <tr>
            <td class="detailedTableLabel">
                Owner
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblOwner_0">Individual</span>
            </td>
            <td class="detailedTableLabel">
                Last Payment
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblLastPaymentDate_0">4/1/2014</span>
            </td>
        </tr>
        <tr>
            <td class="detailedTableAltLabel">
                Contact
            </td>
            <td class="detailedTableAltValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblContact_0">9475 DEEROCO RD SUITE 200 ,TIMONIUM ,MD ,21093</span>
            </td>
            <td class="detailedTableAltLabel">
                Additional Remark
            </td>
            <td class="detailedTableAltValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblRemarks_0">DISPUTE RESOLVED - CUSTOMER DISAGREES</span>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td class="detailedTableLabel">
                Phone/Fax
            </td>
            <td class="detailedTableValue">
                <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_lblPhone_0">8006382003</span>
            </td>
            <td class="detailedTableLabel">
                &nbsp;
            </td>
            <td class="detailedTableValue">
                &nbsp;
            </td>
        </tr>
    </tbody></table>
    <div class="box_M">
        <table width="720" border="0" cellpadding="10">
            <tbody><tr>
                <td>
                    <strong>Payment History</strong>
                </td>
                <td align="right">
                    <img src="../Images/icon_ok.png" style="height:10px;width:10px;">
                    = Current&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;# = Days Late&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                    <img src="../Images/icon_cs.png" style="height:10px;width:10px;">=
                    Critical Status&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                    <img src="../images/icon_na.png" style="height:10px;width:10px;">
                    = No Data Available
                </td>
            </tr>
        </tbody></table>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_0">04/14</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_0" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_0"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_1">03/14</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_1" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_1"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_2">02/14</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_2" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_2"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_3">01/14</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_3" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_3"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_4">12/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_4" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_4"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_5">11/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_5" src="../images/icon_na.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_5"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_6">10/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_6">60</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_7">09/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_7">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_8">08/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_8">60</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_9">07/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_9">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_10">06/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_10" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_10"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_11">05/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_11" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_11"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_12">04/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_12" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_12"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_13">03/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_13">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_14">02/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_14" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_14"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_15">01/13</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_15">60</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_16">12/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_16">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_17">11/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_17">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_18">10/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_18" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_18"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_19">09/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_19" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_19"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_20">08/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_20" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_20"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_21">07/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_21" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_21"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_22">06/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_22" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_22"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_23">05/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_23" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_23"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_24">04/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_24" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_24"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_25">03/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_25" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_25"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_26">02/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_26">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_27">01/12</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_27" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_27"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_28">12/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_28" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_28"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_29">11/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_29" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_29"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_30">10/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_30" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_30"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_31">09/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_31" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_31"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_32">08/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_32" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_32"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_33">07/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_33" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_33"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_34">06/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_34" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_34"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_35">05/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_35" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_35"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_36">04/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_36" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_36"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_37">03/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_37" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_37"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_38">02/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_38" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_38"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_39">01/11</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_39">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_40">12/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_40" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_40"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_41">11/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_41" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_41"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_42">10/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_42">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_43">09/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_43" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_43"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_44">08/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_44">30</span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_45">07/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_45" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_45"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_46">06/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_46" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_46"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_47">05/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">
                        <img id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_imgStatus_47" src="../Images/icon_ok.png">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_47"></span>
                    </div>
                </div>

                <div style="width: 60px; float: left; text-align: center; padding-bottom: 15px;">
                    <div style="background-color: #EAEAEA; border: 1px solid #fff; padding: 3px;">
                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblDate_48">04/10</span></div>
                    <div style="background-color: #F0F5FF; border: 1px solid #fff; padding: 3px;">

                        <span id="MainContent_laInstallments_rprAccount_DetailAccountCntrl1_0_rprPaymentHistory_0_lblStatus_48">60</span>
                    </div>
                </div>

        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
</div>


                </td>
            </tr>

            </tbody></table>










  </div>

</div>


<div id="light2" class="white_content" align="center">

   <h3 style="padding-top: 5%; color: black">Search</h3>
      <script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>



    <div id="light" class="black_content" align="center">



<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>

  </div>



  <div class="footer-section">
    <div class="container">
    <p class="footer-text">Equity Life 2014. All Rights Reserved</p>


        <li><img src="../images/googleplay.png"></li>
        <li><img src="../images/apple.png"></li>




  <ul class="social-box">



        <li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
        <li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
        <li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


    <li><a href="#" class="social-icon">&#62217;</a></li>
    <li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
    <li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
    <li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>

    </ul>


    </div>
  </div>





</body>

</html>
