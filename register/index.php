<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=550, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <link rel="stylesheet" href="../css/styles.css">

    <link rel="stylesheet" href="../src/css/jquery.flipster.css">
    <link rel="stylesheet" href="../css/flipsternavtabs.css">
    <link rel="stylesheet" href="../css/register.css">
    <link href="../assets/css/style.css" rel="stylesheet" />




</head>

<body>

<style>

  .black-input{
    border: 2px solid black!important;
  }

	label{
		font-family: 'Mission Gothic', sans-serif;
		float: left;
	}

	@font-face {
		font-family: 'Entypo-social'!important;
		src: url('../fonts/entypo-social.ttf')!important;
	}

	body{
		background-color: white;
	}

	.flipster .flipster-nav{
		background-color: #3f3f3f!important;
		border-bottom: solid 2px #AF0014!important;
	}

	#answers{
		margin: 0 auto;
		width: 600px;
	}

	input[type="radio"]{
		float: left;
	}

	.footer-text{
		margin-left: 55px;
	}

	.flip-past{
		opacity: 0.3;
	}

	.flip-future{
		opacity: 0.3;
	}


</style>




<div class="top-bar">
	<div class="container">
		<a href="http://equitylife.org/welcome"><img src="../img/logo.png"></a>
    <center style="position: absolute;top: 21px;margin: 0 auto;right: 320px;">
    </center>
		<a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'"></a>
		<nav>
			<a href="#" id="menu-icon"></a>
			<ul>
			<li><a href="http://equitylife.org/apply" class="su-li">Apply Now</a></li>
			<li><a href="http://equitylife.org/who-we-are">Who We Are</a></li>
			<li><a href="http://equitylife.org/our-focus">Our Focus</a></li>
			<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
			<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
			<li><a href="http://equitylife.org/citizenship">Citizenship</a></li>
			<li><a href="http://equitylife.org/lifestyle-categories">Lifestyle</a></li>
			<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>			</ul>
		</nav>
		<a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
	</div>
</div>





		<div class="container" align="center" style="padding-top: 200px;min-height:600px;">

      <form style="width: 50%;border: solid 10px black;padding-top: 30px;" name="contact_form" id="contact_form" method="post" action="/functions/registeruser.php" enctype="multipart/form-data" >

  			<table>
          <tr>
            <td style="vertical-align: top;width:50%;text-align:center;">
              <img src="../img/karo-stroke.png" style="width:150px;float: left;
margin-right: 55px;
margin-top: 28px;">
              <img src="../img/line.png" style="width: 4px;float: right;height: 225px;margin-right: 31px;">
            </td>
            <td>
              <div style="vertical-align: top;">
                <input type="text" name="firstname" id="firstname" class="_input black-input" placeholder="First Name" />
                <br />
                <input type="text" name="lastname" id="lastname" class="_inpu black-input" placeholder="Last Name" />
                <br />
                <input type="text" name="email" id="email" class="_input black-input" placeholder="Email" />
                <br />
                <input type="submit" name="Register" value="Register" class="button" />
              </div>
            </td>
          </tr>
        </table>




      </form>

		</div>







	</div>
</div>



<div class="footer-section">
		<div class="container">
		<p class="footer-text">Equity Life 2014. All Rights Reserved</p>


				<li><img src="http://equitylife.org/images/googleplay.png"></li>
				<li><img src="http://equitylife.org/images/apple.png"></li>




	<ul class="social-box">

				<li><a href="http://equitylife.org/staging/corporate" class="footer-links">Corporate</a></li>
				<li><a href="http://equitylife.org/staging/legal-notices" class="footer-links">Legal Notices</a></li>
				<li><a href="http://equitylife.org/staging/help-and-support" class="footer-links">Help &amp; Support</a></li>


		<li><a href="#" class="social-icon"></a></li>
		<li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon"></a></li>
		<li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon"></a></li>
		<li><a href="#" class="social-icon"></a></li>

		</ul>


		</div>
	</div>



<script>

  $(function(){

    $("#contact_form").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(d, textStatus, jqXHR)
            {
              var data = JSON.parse(d);
                if(data[0].result == "true")
                  window.location = "/welcomeuser/index.php?uname=" + data[0].data;
                else
                  alert("The username or the password is already registered.");
                //data: return data from server
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                //if fails     
            }
        });
        e.preventDefault(); //STOP default action
        //e.unbind(); //unbind. to stop multiple form submit.
    });



  });

</script>


<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('5432-494-10-4139');/*]]>*/</script><noscript><a href="https://www.olark.com/site/5432-494-10-4139/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->





<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="../src/js/jquery.flipster.js"></script>

<!-- JavaScript Includes -->

<script src="../assets/js/jquery.knob.js"></script>

<!-- jQuery File Upload Dependencies -->
<script src="../assets/js/jquery.ui.widget.js"></script>
<script src="../assets/js/jquery.iframe-transport.js"></script>
<script src="../assets/js/jquery.fileupload.js"></script>

<!-- Our main JS file -->
<script src="../assets/js/script.js"></script>


<script>



</script>
</body>
</html>
