<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- CHIEF CREATIVE 2014
  ================================================== -->
	<meta charset="utf-8">
	<title>What We Love - Equity Life</title>
	<meta name="description" content="Chief Creative Labs">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<link rel="stylesheet" href="../stylesheets/grid.css">
	<link rel="stylesheet" href="../stylesheets/media.css">
	<link rel="stylesheet" href="../stylesheets/styles.css">
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.ico">

</head>
<body>

	
<div class="top-bar">
<div class="container">

	<a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>
	

	<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
			<nav>
		
		<a href="#" id="menu-icon"></a>
				<ul>
				<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
				<li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>	
				<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>				
				<li><a href="http://equitylife.org/innovation">Innovation</a></li>		
				<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
				<li><a href="http://equitylife.org/what-we-love">What We Love</a></li>
				
							</ul>
			
					</nav>
		
		
		
	

		
				<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
			
		
		
		
</div>

</div>

<div class="what-we-love">

	<div class="middle">
			<h1>What we love</h1>
		<p class="large-text white">Credit is a lifestyle, not a number. We would love to make a positive impact in our community.</p>
		<li><a href="http://equitylife.org/apply" class="button2">Apply Now</a></li>
<li><a href="http://equitylife.org/who-we-are" class="button2">Who We Are</a></li>

	</div>
	
</div>


	<div class="container">
		<div class="margin">
		<h3 class="add-bottom mission-gothic">FINANCIAL EDUCATION</h3>
	<p class="large-text">Our team of credit specialists will conduct weekly seminars at local education institutions. A lack of financial literacy at a young age yields bad spending habits, which has a negative effect on credit scores as the child enters adulthood. Our specialists would sponsor high school finance clubs and mentor leaders in the school to educate the students in elementary school to college. The goal is to give children the financial knowledge, which will allow them to rise above the poverty line and maintain a healthy financial profile throughout their life. We believe that as majority of the crimes are financially motivated; offering a means to legally and safely obtain that financial end through education is beneficial to both the individual and the community as a whole.</p>
	
	
	</div>
	</div>
	

<div id="light2" class="white_content" align="center">
	
 	<h3 style="padding-top: 5%; color: black">Search</h3>
			<script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
	
 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>
	
	
		<div id="light" class="black_content" align="center">
	
 

<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>
  
	</div>




	<div class="footer-section">
		<div class="container">
		<p class="footer-text">Equity Life 2014. All Rights Reserved</p>
	
							
				<li><img src="../images/googleplay.png"></li>
				<li><img src="../images/apple.png"></li>
			

				
		
	<ul class="social-box">
						
	
			
				<li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
				<li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
				<li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


		<li><a href="#" class="social-icon">&#62217;</a></li> 
		<li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
		<li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
		<li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>
		
		</ul>


		</div>
	</div>




	
</body>

</html>