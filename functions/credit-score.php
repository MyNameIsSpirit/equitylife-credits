<?php

  require_once ('soapclient/SforcePartnerClient.php');
  define('USERNAME', 'joelperodin@equitylife.org');
  define('PASSWORD', 'jimbeam88');
  define('SECURITY_TOKEN', 'wrS5g4fSZNlFJvYfyCNCY1Ml4');

  date_default_timezone_set('UTC');

  //initial config
  function init() {
    $mySforceConnection = new SforcePartnerClient();
    $mySforceConnection->createConnection('soapclient/partner.wsdl.xml');
    $mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);

    //NewCredit($mySforceConnection);
    //creditType($mySforceConnection);
    //AmountsOwed($mySforceConnection);
    LengthCredit($mySforceConnection);
  }

  //trigger app
  init();

  /*
  New Credit
  10% of the credit score
  50 points
  */
  function NewCredit($mySforceConnection) {

    $query = "SELECT Id, Identifier__c, date__c FROM New_Credit__c WHERE Credit_Type__c != 'Mortgage' ORDER BY date__c ASC ";

    $response = $mySforceConnection->query($query);
    $queryResult = new QueryResult($response);

    $previous;
    $current;
    $count = 0;
    $result = 0;
    $evaluated = false;

    for ($queryResult->rewind(); $queryResult->pointer < $queryResult->size; $queryResult->next()) {
        $record = $queryResult->current();

        $size = $queryResult->size;

        if( !$evaluated ){
          //$result = $queryResult->size * 10;
          $result = 50;
          $evaluated = true;
          echo "valor inicial " . $result . "<br/>";
        }
        $result -= 10;

        if (!$current) {
          $current = date_create($record->fields->date__c);
          //$count = 1;
        } else {
          $previous = $current;
          $current = date_create($record->fields->date__c);
          $diff = date_diff($previous, $current);
          $diffMonths = $diff->format('%a') / 30;

          echo "evaluating credit for " . $current->format('Y-m-d H:i:s') . "<br/>";
          echo "diff " . $diffMonths . "<br/>";


          $considerer = false;
          if($diffMonths >= 12) {
            $result+=5;
            $considerer = true;
          }
          else if($diffMonths >= 6) {
            $result+=5;
            $considerer = true;
          }


          if ($diffMonths < 3) {
            $count++;
          } else {
            if(!$considerer){
              $count++;
              echo "contador " . $count;
              $temp = $count * 5;
              $result += $temp;
              $count = 0;
            }
          }

          if ($queryResult->pointer == ($queryResult->size - 1)) {
            $dateNow = new DateTime();

            $diff = date_diff($current, $dateNow);
            $diffMonths = $diff->format('%a') / 30;

            if($diffMonths >= 6) {
              $result+=5;
            }
            if($diffMonths >= 12) {
              $result+=5;
            }

            if ($diffMonths >= 3) {
              $count++;
              $temp = $count * 5;
              $result += $temp;
            }

          }


      }
      echo "resultado " . $result . "<br/> <hr />";

    //echo json_encode($stack);
    }

    if ($result < 0) {
      $result = 0;
    }

    echo "New Credit: " . $result;
  }//closing newCredit

  /*
  Credit type
  10% credit score
  50 points
  */
  function creditType($mySforceConnection) {

    $query = "SELECT SUM(amount__c) amount__c, loans__c FROM Credit_Type__c Group By loans__c ";

    $response = $mySforceConnection->query($query);
    $queryResult = new QueryResult($response);

    $result = 0;

    for ($queryResult->rewind(); $queryResult->pointer < $queryResult->size; $queryResult->next()) {
        $record = $queryResult->current();

        echo "<br>" . $record->fields->loans__c . " amount: " . $record->fields->amount__c;

        if ($record->fields->loans__c == "Installment loan")
        {
          if ($record->fields->amount__c <= 2) {
            $result += $record->fields->amount__c * 20;
          } else {
            $result += 2 * 20;
          }
        }

        if ($record->fields->loans__c == "Revolving loans")
        {

          if ($record->fields->amount__c > 0 && $record->fields->amount__c <= 4) {
            $result += 10;
          } else if ($record->fields->amount__c > 4) {
            $excess = $record->fields->amount__c - 4;
            $result += 10;
            $result -= $excess * 5; //find penalty for each extra credit
          }
        }

        if ($record->fields->loans__c == "Finance loans")
        {
          $result -= $record->fields->amount__c * 10; //find penalty for each finance loan
        }
    }

    echo "<br>Credit Type result: " . $result;
  }

  /*
  Amounts Owed
  30% of credit score
  150 points
  */
  function AmountsOwed($mySforceConnection) {

    $total;
    $result;

    $query = "SELECT Amount__c,Paid__c FROM Amounts_Owed__c";
    $response = $mySforceConnection->query($query);
    $queryResult = new QueryResult($response);

    for ($queryResult->rewind(); $queryResult->pointer < $queryResult->size; $queryResult->next()) {
        $record = $queryResult->current();
        $total += $record->fields->Amount__c; //8500
    }

    for ($queryResult->rewind(); $queryResult->pointer < $queryResult->size; $queryResult->next()) {
        $record = $queryResult->current();
        $totalPercentage = ($record->fields->Amount__c / $total ) ;  //8000 / 8500 || 500 / 8500
        $paidRate = $record->fields->Paid__c / $record->fields->Amount__c ; //0.4125 || 0.328
        $totalEarn = $paidRate * $totalPercentage; //
        $result += $totalEarn * 150; //58.23 + 2.89
    }

    echo "<br>Amounts Owed result: " . $result;
  }

  function LengthCredit($mySforceConnection) {
    $query = "SELECT StartDate__c, EndDate__c FROM LengthCredit__c ORDER BY StartDate__c ASC NULLS FIRST";
    for ($queryResult->rewind(); $queryResult->pointer < $queryResult->size; $queryResult->next()) {
        $record = $queryResult->current();

    }


  }

?>
