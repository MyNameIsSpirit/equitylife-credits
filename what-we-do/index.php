<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

  <!-- CHIEF CREATIVE 2014
  ================================================== -->
  <meta charset="utf-8">
  <title>Corporate - Equity Life</title>
  <meta name="description" content="Chief Creative Labs">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


  <link rel="stylesheet" href="../stylesheets/grid.css">
  <link rel="stylesheet" href="../stylesheets/media.css">
  <link rel="stylesheet" href="../stylesheets/styles.css">
  <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>


  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="../images/favicon.ico">

</head>
<body>


<style>
.container .nine.columns{
  width: 470px;
}
p.question {
font-weight: bold;
color: black;
}
p {
margin: 0 0 20px 0;
color: rgb(68, 68, 68);
}

hr{
  border: solid black;
  width: 80%;
margin: 0 auto;
margin-bottom: 18px
}
.phone{
  background: url('../images/phonewhite.jpg') no-repeat;
height: 450px;
background-position-x: 22px;
}
.phone:hover{
  background: url('../images/phoneblack.jpg') no-repeat;
height: 450px;
background-position-x: 22px;
}
</style>

<div class="top-bar">
<div class="container">

  <a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>


  <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
      <nav>

    <a href="#" id="menu-icon"></a>
        <ul>
        <li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
        <li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>
        <li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
        <li><a href="http://equitylife.org/innovation">Innovation</a></li>
        <li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
        <li><a href="http://equitylife.org/what-we-love">What We Love</a></li>

              </ul>

          </nav>






        <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>




</div>

</div>



<div class="what-we-do">
<div class="container">
  <div class="" style="padding-top: 20%;" align="center">
    <h1>CONCOUR'S D'ELEGANCE</h1>

      </div>




</div></div>
<div class="container" style="position:relative">



  <div style="margin: 0 auto;width: 75%;">

    <div class="column-left">

      <hr />

      <p class="question">SIGN UP AND START REFERRING TODAY.</p>

      <p>- Just provide your name and email address. (Currently only available in the US )</p>

      <p class="question">WHAT YOU'LL NEED</p>

      <p> - A valid email student ex. larry@equitylife.edu.</p>

      <p>- A bank account or a debit card to receive direct deposits.</p>

      <p>No experience required, no systems to manage; just an interest in helping people experience a better way to manage their
        credit.</p>

      <p class="question">JOIN</p>

      <p>Tell your family, friends or anyone else about Equity Life.</p>

      <p>Referring is quick and simple:</p>

      <p>- Receive a unique referral link after registering for the program</p>

      <p>- Start with email and phone number you register with your membership.</p>

      <p class="question">SHARE</p>

      <p>Receive $100 for every user that signs up and pays for his/her membership.</p>

      <p class="question">Earn your rewards</p>

      <p>- $100/user, paid directly to your bank account</p>

      <p>- Coupons that save your referrals $125/user for the first year</p>

      <p>- More coupons as you continue referring</p>


    </div>

    <div class="column-center">
      <img src="../images/cam.png" />

      <hr />

      <table>
        <tr class="phone">
          <td style="padding-top: 80px;
padding-left: 70px;
padding-right: 69px;
font-size: 28px;
text-align: left;
font-weight: bold;
color: white;">
            <span>REFER 5 NEW MEMBERS BY OCTOBER 5TH AND RECIEVE A NEW IPHONE 6</span>
            </td>
        </tr>
      </table>

    </div>

    <div  class="column-right">
      <p>The first-ever Ultimate Giveaway Prize and experience

additional excitement from every single member.</p>

<p>Your recommendations could help you win one of our

top three prizes and you could even have your college

tuition paid for an entire year!<p>

<p class="question">THE FINE PRINT</p>

<p>- You can refer an unlimited number of customers.</p>

<p>- You're rewarded for each referral you send to

 Equity Life.</p>

<p>- Your referral amount will be based on the number

 of users who have paid for Equity Life's

 Credit Expert Program.</p>

<p>- Currently only available in the US.</p>
<hr />
<p>This is part time work for extra cash.

No experience necessary. You need

to sign up members for a product that

costs $500 but you sell this product at a

discounted price of $250.

You will be paid $100 for each new paid

member.</p>

<p>If you get 3 new members, you will receive

a GoPro Black Edition and when you refer 5

you will receive a Iphone 5 by October 5th,

2014 as a bonus. Once you sign up 100 new

clients, you will have a chance to become a

coach and get valuable training after you

pass our exam. After you have sold 100

memberships, your commission rate changes

to $75.00 per paid membership. However, you

still are eligible to receive a new $50,000 once

you have signed up 1,000 new paid members.

Once you have 1,500 paid memberships, you

will receive $75,000 and once you have reach

2,000 paid memberships, will receive a $100,000.

To be eligible for these bonuses, you must achieve

 these sales goal by December 15, 2014.</p>
    </div>


  </div>



</div>


<div id="light2" class="white_content" align="center">

   <h3 style="padding-top: 5%; color: black">Search</h3>
      <script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>



    <div id="light" class="black_content" align="center">



<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>

  </div>



  <div class="footer-section">
    <div class="container">
    <p class="footer-text">Equity Life 2014. All Rights Reserved</p>


        <li><img src="../images/googleplay.png"></li>
        <li><img src="../images/apple.png"></li>




  <ul class="social-box">



        <li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
        <li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
        <li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


    <li><a href="#" class="social-icon">&#62217;</a></li>
    <li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
    <li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
    <li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>

    </ul>


    </div>
  </div>





</body>

</html>
