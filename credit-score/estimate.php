<html>

<head>

  <!-- CHIEF CREATIVE 2014
  ================================================== -->
  <meta charset="utf-8">
  <title>Welcome - Equity Life</title>
  <meta name="description" content="Chief Creative Labs">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


  <link rel="stylesheet" href="../stylesheets/grid.css">
  <link rel="stylesheet" href="../stylesheets/media.css">
  <link rel="stylesheet" href="../stylesheets/styles.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="../js/formulas/ScoreEstimator.js"></script>



  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="images/favicon.ico">

</head>
<body>
<div class="top-bar">
<div class="container">

  <a href="http://equitylife.org/welcome"><img src="images/logo.png"></a>
  <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>

  <nav>
    <a href="#" id="menu-icon"></a>
      <ul>
        <li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
        <li><a href="http://equitylife.org/innovation">Innovation</a></li>
        <li><a href="http://equitylife.org/credit-insight">Insight</a></li>
        <li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
        <li><a href="http://equitylife.org/challenge">Course</a></li>
        <li><a href="http://equitylife.org/community">Community</a></li>
        <li><a href="http://equitylife.org/credit-focus">Credit Focus</a></li>
      </ul>
    </nav>
    <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
</div>

</div>
  <div class="container">
    <div class="margin">
      <!--starts-->
      <div id="estimate_mainContent">
    <div id="tabbed_data">
      <table cellpadding="0" cellspacing="11">
        <tbody><tr><td class="first header">Your Credit Behavior</td><td class="second header">As of 09/24/2014</td><td class="header">Potential</td></tr>
        <tr>
          <td class="firstcolumn">What percent of your available credit are you using?</td>
          <td class="secondcolumn">72%</td>
          <td>
            <select id="ecdPercentCreditUsage" onchange="CalculateScore()">
              <option value="15">0-15%</option>
              <option value="29">16-29%</option>
              <option value="50">30-50%</option>
              <option value="64">51-64%</option>
              <option value="100">65%+</option>
            </select>
          </td>
        </tr>
        <tr><td class="firstcolumn">How many Hard Inquiries do you have?</td>
        <td class="secondcolumn">3</td>
        <td>
          <select id="ecdNumOfInquiries" onchange="CalculateScore()">
            <option value="0">0</option>
            <option value="2">1-2</option>
            <option value="4">3-4</option>
            <option value="6">5-6</option>
            <option value="7">7+</option>
          </select>
        </td></tr>
        <tr><td class="firstcolumn">What is your number of open Installment Loans?</td>
        <td class="secondcolumn">2</td>
        <td>
          <select id="ecdNumOfInstallments" onchange="CalculateScore()">
            <option value="0">0</option>
            <option value="2">1-2</option>
            <option value="4">3-4</option>
            <option value="5">5+</option>
          </select>
        </td></tr>
        <tr><td class="firstcolumn">Have you declared a new bankruptcy?</td>
        <td class="secondcolumn">No</td>
        <td>
          <select id="ecdBankruptcyFiling" onchange="CalculateScore()">
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
        </td></tr>
        <tr><td class="firstcolumn">Do you have any delinquent accounts now?</td>
        <td class="secondcolumn">No</td>
        <td>
          <select id="ecdHasDelinquentAccnt" onchange="CalculateScore()">
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
        </td></tr>
        <tr><td class="firstcolumn">Do you have a mortgage?</td>
        <td class="secondcolumn">No</td>
        <td>
          <select id="ecdHasMortgage" onchange="CalculateScore()">
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
        </td></tr>
      </tbody></table>
      <div id="ResetEstimator" style="cursor: pointer;text-align: right;margin-right: 30px;">
        <img onclick="javascript:renderSummarySection('3');" src="/Themes/2/Pages/ScoreCenter/Images/btn_reset.png" alt="" border="0"></div>
    </div>

    <div id="outerBox">
      <div id="innerBox">
        <div id="scoreHeader">Your Current Score is:</div>

        <div id="upImage" class="scoreImg" style="display:none"><img src="/Themes/2/Pages/ScoreCenter/Images/score_inc.jpg" alt="" border="0" style="height:58px;width:58px;"></div>
        <div id="downImage" class="scoreImg" style="display:none"><img src="/Themes/2/Pages/ScoreCenter/Images/score_dec.jpg" alt="" border="0" style="height:58px;width:59px;"></div>
        <div id="equalImage" class="scoreImg" style="display:none"><img src="/Themes/2/Pages/ScoreCenter/Images/score_equal.jpg" alt="" border="0" style="height:58px;width:59px;"></div>

        <div id="estimatedScore">638</div>
      </div>

      <div id="scoreUp" class="scoreDiff" style="display:none">Your Score could increase an estimated<br> <span><span id="scoreDiffPos">0</span> points. </span></div>
      <div id="scoreDown" class="scoreDiff" style="display:none">Your Score could decrease an estimated<br> <span><span id="scoreDiffNeg">0</span> points. </span></div>
      <div id="scoreEqual" class="scoreDiff" style="display:none">No change</div>
    </div>


    <div id="disclaimer">
    <strong>Disclaimer</strong><br>
    The Score Estimator uses select factors to which consumers can usually relate so you can get a better understanding about how your score may have been influenced by your credit behavior.<br><br>
    Actual Credit Score are comprised of many different factors found within your Credit Report and each factor can affect your Credit Score differently.  Depending on the information in your current Credit Report, each of the factors used here may or may not have an affect on your actual Credit Score, since the overall effect these factors have on a score varies form person to person.<br><br>


* Calculated on the PLUS Score model, your Experian Credit Score indicates your relative credit risk level for educational purposes and is not the score used by lenders. <a href="javascript:void(0);" onclick="javascript:exitvariable=false;popUp('Message.aspx?PageTypeID=PlusScoreLearnMore&amp;nav=false&amp;SiteVersionID=1003&amp;SiteID=100345&amp;Status=A&amp;sc=675834&amp;bcd=xxxxxxx-LG-hmp-mq-xxxx-xx-xx-xxxxxxxx-dx-0003&amp;cid=7146&amp;navflowid=1159&amp;MVTCampaignID=OTR74891%2bEXPN%2bCredit.com%252f3B%252fECA%2bAds%2b(BlackCard)%2bDEFAULT%2b140312&amp;MVTRecipeID=B%2bCredit%252f3B%252fECA&amp;custnum=151360375','PlusScoreDisclaimer','titlebar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,width=600,height=320');">Learn More</a>.<p></p>


    </div>

  </div>



      <!-- ends -->
    </div>
  </div>

  <div class="footer-section">
    <div class="container">
      <p class="footer-text">Equity Life 2014. All Rights Reserved</p>
      <li><img src="images/googleplay.png"></li>
      <li><img src="images/apple.png"></li>

      <ul class="social-box">
        <li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
        <li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
        <li><a href="http://equitylife.org/help-and-support" class="footer-links">Help & Support</a></li>
        <li><a href="#" class="social-icon">&#62217;</a></li>
        <li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
        <li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
        <li><a href="#" class="social-icon">&#62253;</a></li>
      </ul>

    </div>
  </div>



<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('5432-494-10-4139');/*]]>*/</script><noscript><a href="https://www.olark.com/site/5432-494-10-4139/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>


  </body>
</html>
