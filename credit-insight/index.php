<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- CHIEF CREATIVE 2014
  ================================================== -->
	<meta charset="utf-8">
	<title>Credit Insight - Equity Life</title>
	<meta name="description" content="Chief Creative Labs">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<link rel="stylesheet" href="../stylesheets/grid.css">
	<link rel="stylesheet" href="../stylesheets/media.css">
	<link rel="stylesheet" href="../stylesheets/styles.css">
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="../images/favicon.ico">

</head>
<body>

	
<div class="top-bar">
<div class="container">

	<a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>
	

	<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
			<nav>
		
		<a href="#" id="menu-icon"></a>
				<ul>
				<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
				<li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>	
				<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>				
				<li><a href="http://equitylife.org/innovation">Innovation</a></li>		
				<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
				<li><a href="http://equitylife.org/what-we-love">What We Love</a></li>
				
							</ul>
			
					</nav>
		
		
		
	

		
				<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
			
		
		
		
</div>

</div>



<div class="credit-insight">
<div class="margin">
<div class="black-section" align="left">
<div style="height:60%;100%; overflow:auto;">
<h4 class="white">WHAT IS A CREDIT SCORE?</h4>
	<p class="white">
A credit score is a three-digit number. Credit scores range from 280 to 850, which credit bureaus calculate based on information in your credit report. It is a simple, numeric expression of your credit worthiness. A good credit score is typically 660 or higher (very good is above 725; excellent is above 760). Although the three credit reporting bureaus (Equifax, Experian, and Trans Union), use similar methods to determine a credit score, the formulas they use are not exactly the same and your credit score will vary from bureau to bureau.</p>
<img src="../images/goodc.png">
<p class="white">Credit scores play an important role in today?s digital economy. The interest rate you get on a home or car loan, for example, is determined by your credit score. Also, when you apply for a credit card or a charge card at a retailer, the answer depends largely on your score. You should regularly check your credit score.

Credit scores play an important role in today?s digital economy. The interest rate you get on a home or car loan, for example, is determined by your credit score. Also, when you apply for a credit card or a charge card at a retailer, the answer depends largely on your score. You should regularly check your credit score.</p>

<h4 class="white">The main factors involved in calculating your credit score are: </h4>
<p class="white">The number of accounts you have.</p>

<p class="white">The types of accounts.</p>

<p class="white">Your available credit.</p>

<p class="white">The length of your credit history.</p>

<p class="white">Your payment history.</p>
<h4 class="white">how is my credit score calculated?</h5><p class="white">
Your credit score is calculated based on a number of factors in your credit history, including the number and type of credit accounts you have, the amount of available credit, the length of your credit history and how you have paid your bills. Each of these factors is assigned a numerical value, and then weighted to determine your credit score.
</p>
<img src="../images/pie.png">

<h4 class="white">how do my actions impact my credit score?</h5><p class="white">
The good news is that no matter where your credit score is today, there are a number of different steps you can take now that can change your credit history and help impact your credit score. You should take all the steps you can to help establish a good credit score.</p>
<img src="../images/goodchart.png">
<p class="white">
How old are my credit accounts? The longer you have had a credit account open and active, the more likely it will help stabilize your credit history and positively impact your credit score.</p>

<p class="white">
Do I have enough different types of credit accounts? Ideally, you will have at least four trade lines of different types, such as credit cards, student loans, a mortgage or home equity line of credit or perhaps an auto loan.</p>
<p class="white">
Are my balances too high relative to my total available credit limit? Creditors prefer to see a lower ratio of how much debt you?re carrying compared with how much available credit you have on a particular account. Ideally, you?ll use less than 35% of the total credit limit on any particular account. For example, if your credit card has a maximum credit limit of $1,000, carrying a balance above $350 on that card could negatively affect your credit score.</p>
<p class="white">
Do I have any judgments, liens, foreclosures, bankruptcies, short sales or delinquencies that have been reported to creditors? Having this sort of information on your credit history is extremely damaging to your credit score. If you have gone through a reversal of fortune, and had to file for bankruptcy or completed a foreclosure, your credit score will reflect this negative information for several years.
</p>

<h4 class="white">why should i check my credit history and credit score?</h5>

<p class="white">
In today's digital economy, your credit history and credit score are vital pieces of information that are key to helping you secure your financial life. Credit card companies, mortgage lenders, and insurance companies will pull copies of your credit report and score in order to decide whether to extend credit or how much to charge for your insurance premium.
</p><p class="white">
Financial services companies tend to group borrowers into segments according to their credit score. These credit score ranges may determine how much you?ll be charged for your insurance coverage or the interest rate you pay on your mortgage, student or car loan or the type of credit card you?ll be offered.</p><p class="white">

Typically, if your credit score falls in the uppermost range, most lenders and creditors will consider you to be an excellent credit risk, and may extend the best credit offers to you. If your score is in the lowest range, it may be extremely difficult for you to obtain a loan on any terms whatsoever.
</p><p class="white">
You should check your credit history and credit score regularly, so that you know where you stand before you apply for a credit card, a car or home loan or even auto insurance. That way, you can decide whether to pursue a credit application now or wait until your credit history and credit score improve.

</p>
</div>
</div>

			</div>




</div></div>













<div class="container">

<div class="margin">



<div class="nine columns right" style="margin-left: 0px;">
<h4>What Is A Credit Report?</h4>
<p>A credit score is a three-digit number. Credit scores range from 280 to 850, which credit bureaus calculate based on information in your credit report. It is a simple, numeric expression of your credit worthiness. A good credit score is typically 660 or higher (very good is above 725; excellent is above 760). Although the three credit reporting bureaus (Equifax, Experian, and Trans Union), use similar methods to determine a credit score, the formulas they use are not exactly the same and your credit score will vary from bureau to bureau. </p>
<img src="../images/credit-report.png">

</div>

<div class="nine columns" style="margin-right: 0px;">
<h4>How Does Information Get On
My Credit Report?</h4>
<p>The three national credit reporting bureaus (Equifax, Experian and TransUnion) maintain a history of your credit activity.</p>
<p>
The credit bureaus get their information from credit card companies, banks, retailers, mortgage companies and other lenders that have granted you credit, and then each of the bureaus compiles the information into your consumer credit report. Information from public records and collection agencies is also reported to the credit reporting bureaus and then listed in your credit report.
</p>


<p>
The three national credit reporting bureaus (Equifax, Experian and TransUnion) maintain a history of your credit activity.</p><p>

The credit bureaus get their information from credit card companies, banks, retailers, mortgage companies and other lenders that have granted you credit, and then each of the bureaus compiles the information into your consumer credit report. Information from public records and collection agencies is also reported to the credit reporting bureaus and then listed in your credit report.
 </p>

</div>

</div>

<hr>

</div><div class="container">

<div class="nine columns right" style="margin-left: 0px;">
<h4>How Often is My Credit Report Updated</h4>
<p>Your credit score is calculated based on a number of factors in your credit history, including the number and type of credit accounts you have, the amount of available credit, the length of your credit history and how you have paid your bills. Each of these factors is assigned a numerical value, and then weighted to determine your credit score.</p>

</div>

<div class="nine columns" style="margin-right: 0px;">
<p>If you regularly make on-time payments and fulfill the terms of your payment agreement, that positive information will be listed on your credit report and help raise your credit score.</p>
<p>
If you make late payments or miss payments altogether, that negative information will also be listed on your credit report and will be a negative factor when calculating your credit score.</p>
</div><hr>
</div>


</div>

<div class="container">

<div class="nine columns right" style="margin-left: 0px;">
<h4>How Long Do Late Payments and Other Negative Information Stay on My Credit Report?</h4>
<p>Fortunately, there are set timeframes for how long different types of negative information can remain on your credit report. Late payments stay on your credit report for approximately seven years, for example, as do most other types of negative information, including judgments and paid tax liens. Bankruptcies, however, can stay on your credit report for up to 10 years.</p></div>

<div class="nine columns" style="margin-right: 0px;">
<p>

Positive information generally remains on your credit report for a longer period of time and can therefore help to improve your creditworthiness and boost your credit score. In general, credit accounts that you have paid as agreed will stay on your credit report for up to 10 years. If you have a revolving credit account, such as a credit card, that has been paid as agreed, it can stay on your credit report forever ? as long as you keep the account open. 
Regularly check your credit report to ensure that negative information falls off after the appropriate amount of time has elapsed. You can order one free credit report from each of the three national credit reporting bureaus once a year.</p>


</div>
<hr>
</div>


</div>
<div class="container">
<div class="nine columns right" style="margin-left: 0px;">
<h4>How Do I Ensure My Credit Report is Accurate?</h4>

<p>Erroneous information could wind up in your credit report if a creditor mistakenly reports inaccurate account information, or if you?ve become a victim of identity theft or fraud. Any inaccurate information in your credit report could lower your credit score and change the way lenders evaluate you. Before you apply for new credit, you?ll want to make sure your credit report is accurate and up to date so you can qualify for the best terms and lowest interest rate.</p>
<p>To ensure that your personal and financial information is accurate, check your credit report. You can order one free copy of your credit report from each of the three national credit reporting bureaus every year.</p>


</div>

<div class="nine columns" style="margin-right: 0px;">
<p>Once you've pulled a copy of your consumer credit report, carefully comb through it for any errors or outdated information. As you search through your credit report, make sure that:</p>
<p>- Your personal information, such as your name and address, is correct;<br>
- All of your credit accounts are listed;<br>
- You recognize each credit account listed;<br>
- Your payment history is correct for each account;<br>
- The balances and account age (the date the account was opened) are correct for each of your accounts;<br>
- Inactive accounts have remained inactive. They shouldn?t show new activity;<br>
- All hard inquiries are for credit accounts that you have applied for;<br>
- All negative information, such as late payments, bankruptcies, liens and judgments, has fallen off of your credit report after the appropriate amount of time has elapsed;</p>
</div>
<hr>
</div>



<div class="container">
<div class="nine columns right" style="margin-left: 0px;">
<h4>How Can I Dispute Errors On
My Credit Report?</h4>
<p>If you do spot an error in your credit report, you can file a free dispute with each of the credit reporting bureaus. You can file a dispute with Equifax either online, by phone or by mail. By law, Equifax is required to investigate your dispute within 30 to 45 days and inform you of the outcome. If you file your dispute by mail, you will be notified of the outcome by mail. If you file your dispute online, you will receive email updates throughout the investigation.</p>

<p>
After your dispute has been investigated, Equifax will either update the status of the disputed information (this may include informing you that the creditor verified the information it was reporting was correct) or delete the disputed item from your file. If the creditor confirms that the disputed information is correct and will remain in your credit report, you have the option to add a statement of explanation to your file, free of charge, in order to explain the nature of your dispute.
</p>


</div>

<div class="nine columns" style="margin-right: 0px;">
<p>If you find inaccurate information in your Equifax credit report, you should consider ordering a copy of your credit report from the other two credit reporting bureaus to see if they are reporting the same mistake. If you correct an error through Equifax?s dispute process, the other two bureaus should eventually receive the corrected information.</p>

<p>To correct the error quicker, though, consider contacting the other two credit reporting bureaus directly to dispute the inaccurate information. You can also contact your creditor to dispute a credit report error.</p>

<p>In order to build a strong credit report and improve your credit score, get in the habit of regularly checking your credit report so you can catch any errors that could affect your credit score.</p>

</div>
<hr>
</div><div class="container" style="margin-bottom: 50px;">
<div class="nine columns right" style="margin-left: 0px;">
<h4>Who is Allowed to See My Credit Report?</h4>
<p>
The Fair Credit Reporting Act (FCRA) governs who can access your credit report. In Section 604, under Permissible Purposes of Consumer Credit Reports, the FCRA lists the ways your credit report might be accessed:</p>
<p>
- Court order or subpoena in conjunction with a Federal grand jury<br>
- Consumer request: you directed in writing that a person be allowed to see it<br>
- Credit application: you have applied for credit with a creditor or lender<br>
- Pre approved credit offers from creditors and lenders<br>
- Insurance coverage evaluation<br>
- Prospective employers<br>
- Government licensing body or agency<br>
- Potential investor<br>
- Family court, to determine child or family support<br>
- Other legitimate business interests<br>
</p>
</div>


<div class="nine columns" style="margin-right: 0px;">
<img src="../images/people.png">

</div>
</div>
	
	</div>

<div id="light2" class="white_content" align="center">
	
 	<h3 style="padding-top: 5%; color: black">Search</h3>
			<script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
	
 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>
	
	
		<div id="light" class="black_content" align="center">
	
 

<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>
  
	</div>




	<div class="footer-section">
		<div class="container">
		<p class="footer-text">Equity Life 2014. All Rights Reserved</p>
	
							
				<li><img src="../images/googleplay.png"></li>
				<li><img src="../images/apple.png"></li>
			

				
		
	<ul class="social-box">
						
	
			
				<li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
				<li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
				<li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


		<li><a href="#" class="social-icon">&#62217;</a></li> 
		<li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
		<li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
		<li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>
		
		</ul>


		</div>
	</div>




	
</body>

</html>