<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=550, initial-scale=1">
    <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/styles.css">

    <link rel="stylesheet" href="src/css/jquery.flipster.css">
    <link rel="stylesheet" href="css/flipsternavtabs.css">
    <link rel="stylesheet" href="css/register.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="js/heartcode-canvasloader-min-0.9.1.js"></script>

</head>

<body>

<style>
  form{
      background: #16849c; /* Old browsers */
      /* IE9 SVG, needs conditional override of 'filter' to 'none' */
      background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzE2ODQ5YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwYzdlOWEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
      background: -moz-linear-gradient(top,  #16849c 0%, #0c7e9a 100%); /* FF3.6+ */
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#16849c), color-stop(100%,#0c7e9a)); /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top,  #16849c 0%,#0c7e9a 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(top,  #16849c 0%,#0c7e9a 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(top,  #16849c 0%,#0c7e9a 100%); /* IE10+ */
      background: linear-gradient(to bottom,  #16849c 0%,#0c7e9a 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#16849c', endColorstr='#0c7e9a',GradientType=0 ); /* IE6-8 */
      width: 75%;
      color: white;
      padding: 10px 20px;
      -webkit-box-shadow: -7px 7px 5px 0px rgba(50, 50, 50, 0.75);
      -moz-box-shadow:    -7px 7px 5px 0px rgba(50, 50, 50, 0.75);
      box-shadow:         -7px 7px 5px 0px rgba(50, 50, 50, 0.75);
      height: 450px;
  }

  label{
    float: left;
  }

  @font-face {
    font-family: 'Entypo-social'!important;
    src: url('fonts/entypo-social.ttf')!important;
  }

  body{
    background-color: white;
  }

  .flipster .flipster-nav{
    background-color: #3f3f3f!important;
    border-bottom: solid 2px #AF0014!important;
  }

  #answers{
    margin: 0 auto;
    width: 600px;
  }

  input[type="radio"]{
    float: left;
  }

  .footer-text{
    margin-left: 55px;
  }

  .flip-past{
    opacity: 0.3;
  }

  .flip-future{
    opacity: 0.3;
  }
.centered {
  position: fixed;
  top: 30%;
  left: 42%;
  margin-top: -50px;
  margin-left: -100px;
}

.centered2{
  position: fixed;
  top: 23%;
  left: 40%;
  margin-top: -50px;
  margin-left: -100px;
}
input[type="text"]{
  background: rgba(255,255,255,0);
  border-left-color: transparent;
  border-top-color: transparent;
  border-right-color: transparent;
  display: inline-table;
  width: 70%
}
</style>




<div class="top-bar">
  <div class="container">
    <a href="http://equitylife.org/welcome"><img src="../img/logo.png"></a>
    <center style="position: absolute;top: 21px;margin: 0 auto;right: 320px;">
    </center>
    <a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'"></a>
    <nav>
      <a href="#" id="menu-icon"></a>
      <ul>
      <li><a href="http://equitylife.org/apply" class="su-li">Apply Now</a></li>
      <li><a href="http://equitylife.org/who-we-are">Who We Are</a></li>
      <li><a href="http://equitylife.org/our-focus">Our Focus</a></li>
      <li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
      <li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
      <li><a href="http://equitylife.org/citizenship">Citizenship</a></li>
      <li><a href="http://equitylife.org/lifestyle-categories">Lifestyle</a></li>
      <li><a href="http://equitylife.org/iqscore">Iqscore</a></li>			</ul>
    </nav>
    <a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
  </div>
</div>



<div class="landingWhite">

    <div class="container" align="center" style="position:relative">


<!--<script src="https://mbsy.co/embed/v2/getcookie/equitylife" type="text/javascript"></script>
<script type="text/javascript">
  var mbsy_web_to_lead_form_id = 'web-to-lead-form'; // Use your own form ID.

  window.onload = function() {
    if (typeof mbsy_short_code != 'undefined') {
      mbsy_form = document.getElementById(mbsy_web_to_lead_form_id);
      var mbsy_shortcode_input = document.createElement('input');
      mbsy_shortcode_input.type = 'hidden';
      mbsy_shortcode_input.name = '00N70000003CuZr';
      mbsy_shortcode_input.id = '00N70000003CuZr';
      mbsy_shortcode_input.value = mbsy_short_code;
      if (mbsy_form) {
          mbsy_form.appendChild(mbsy_shortcode_input);
      }
    }
  }
</script>-->

<div style="margin-left: 25%;
margin: 0 auto;
min-width: 300px;
max-width: 600px;
padding-top: 115px;">



<!--  ----------------------------------------------------------------------  -->
<!--  NOTE: Please add the following <META> element to your page <HEAD>.      -->
<!--  If necessary, please modify the charset parameter to specify the        -->
<!--  character set of your HTML page.                                        -->
<!--  ----------------------------------------------------------------------  -->

<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">

<!--  ----------------------------------------------------------------------  -->
<!--  NOTE: Please add the following <FORM> element to your page.             -->
<!--  ----------------------------------------------------------------------  -->

<form id="web-to-lead-form" action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">

<input type=hidden name="oid" value="00D70000000N2Dl">
<input type=hidden name="retURL" value="http://equitylife.org/thank-you.php">

<!--  ----------------------------------------------------------------------  -->
<!--  NOTE: These fields are optional debugging elements. Please uncomment    -->
<!--  these lines if you wish to test in debug mode.                          -->
<!--  <input type="hidden" name="debug" value=1>                              -->
<!--  <input type="hidden" name="debugEmail"                                  -->
<!--  value="joelperodin@equitylife.org">                                     -->
<!--  ----------------------------------------------------------------------  -->

<label for="first_name">First Name</label><input  id="first_name" maxlength="40" name="first_name" size="20" type="text" /><br>

<label for="last_name">Last Name</label><input  id="last_name" maxlength="80" name="last_name" size="20" type="text" /><br>

<label for="email">Email</label><input  id="email" maxlength="80" name="email" size="20" type="text" /><br>

<input type="submit" name="submit">

</form>


</div>

    </div>


  </div>




  </div>
</div>









<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('5432-494-10-4139');/*]]>*/</script><noscript><a href="https://www.olark.com/site/5432-494-10-4139/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->





<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="src/js/jquery.flipster.js"></script>
<script>


</script>
</body>
</html>
