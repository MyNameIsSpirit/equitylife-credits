<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- CHIEF CREATIVE 2014
  ================================================== -->
	<meta charset="utf-8">
	<title>Legal Notices - Equity Life</title>
	<meta name="description" content="Chief Creative Labs">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<link rel="stylesheet" href="../stylesheets/grid.css">
	<link rel="stylesheet" href="../stylesheets/media.css">
	<link rel="stylesheet" href="../stylesheets/styles.css">
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.ico">

</head>
<body>


<div class="top-bar">
<div class="container">

	<a href="../log-in"><img src="../images/logo.png"></a>


	<a style="display:none" href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
		<nav>

		<a style="display:none" href="#" id="menu-icon"></a>
				<ul>
				<li><a href="http://equitylife.org/apply">Apply Now</a></li>
				<li><a href="http://equitylife.org/citizenship">What We Love</a></li>
				<li><a href="http://equitylife.org/innovation">Innovation</a></li>
				<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
				<li><a href="http://equitylife.org/credit-program">Credit Program</a></li>
				<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
				<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
				<li><a href="http://equitylife.org/lifestyle-categories">Culture</a></li>
							</ul>

					</nav>





				<a style="display:none" href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>




</div>

</div>



	<div class="left-section2">
	<div class="legal-box" align="left;">

	<a href="#security" class="legal-links">Security</a><br>
	<a href="#terms" class="legal-links">Terms of Services</a><br>
	<a href="#privacy" class="legal-links">Privacy</a><br>
	<a href="#faq" class="legal-links">Faq</a><br>
	</div></div>


		<div class="s-box">
		<div id="security" style="margin-bottom: 10%;">
<h4 style="color: black">

SAFE AND SECURE</h4><p>
Our Wealth Management System employs the most advanced security features and protocols to keep your data safe, private and secure, 24/7/365. Comprehensive security protection measures include password protection, secure socket layer encryption, firewalls, intrusion detection, audits, inspections and more. You can be confident that your important information is safe and secure.</p>
<h4 style="color: black">NON TRANSACTIONAL</h3><p>
Unlike online banking, trading or shopping websites, your money cannot be moved, withdrawn or accessed on our system.
<h4 style="color: black">PASSWORD PROTECTION</h3>
- You have a unique User ID and Password. If three consecutive login attempts fail, the system automatically locks your account for 10 minutes, blocking any manual or programmed hacking attempts.
? Your User ID and Password will never be given out over the phone or sent to an email address that is not pre-registered with your account.
<h4 style="color: black">HIGHEST ENCRYPTION</h3><p>
- Our system uses a 256-bit Secure Socket Layer to scramble your data to prevent access to unauthorized users. This is the highest level of encryption currently available today.
- Your data is so scrambled that trying to read it without authorization is like looking for a single grain of sand on miles of beach.</p>
<h4 style="color: black">SECURED DATA</h3><p>
We house your important data with SunGard Data Systems, a revolutionary server hosting space offering the most secure environment in the industry. SunGard hosts 70% of all financial industry transactions.
Physical access at SunGard?s world-class hosting server centers is limited to authorized personnel, and requires multiple levels of authentication, including fingerprint scanning. SunGard Data Systems also makes use of fire protection, electronic shielding, database backups and more to ensure your data is continuously monitored and protected.</p>
<h4 style="color: black">CERTIFIED HACKER SAFE</h3><p>
Our Wealth Management System uses third-party security auditors including McAfee, TraceSecurity, and WhiteHat Security to continuously try to hack into the system. Our firewalls have never been breached.
Our Wealth Management System is Verisign certified to be hacker safe. To receive this approval, the system is updated every 15 minutes with tests for newly discovered vulnerabilities and validated fixes from hundreds of sources worldwide.
		</p>
		</div>









			<div id="terms" style="margin-bottom: 10%;">
<h4 style="color: black">

EquityLife Terms of Service</h4>
<p>
Last modified: July 18, 2014 (view archived versions)
</p><p >
Welcome to Equity Life!
</p><p>
Thanks for using our products and services (?Services?). The Services are provided by  Equity Life and Co Inc. (?Equity Life?), located at 1701 Pennsylvania Ave Ste 900  Washington, DC 20006 United States.
</p><p >
By using our Services, you are agreeing to these terms. Please read them carefully.
<p >
Our Services are very diverse, so sometimes additional terms or product requirements (including age requirements) may apply. Additional terms will be available with the relevant Services, and those additional terms become part of your agreement with us if you use those Services.
</p>
Using our Services
<p >
You must follow any policies made available to you within the Services.
</p>
<p>
Don't misuse our Services. For example, don?t interfere with our Services or try to access them using a method other than the interface and the instructions that we provide. You may use our Services only as permitted by law, including applicable export and re-export control laws and regulations. We may suspend or stop providing our Services to you if you do not comply with our terms or policies or if we are investigating suspected misconduct.
</p><p >
Using our Services does not give you ownership of any intellectual property rights in our Services or the content you access. You may not use content from our Services unless you obtain permission from its owner or are otherwise permitted by law. These terms do not grant you the right to use any branding or logos used in our Services. Don?t remove, obscure, or alter any legal notices displayed in or along with our Services.
</p><p >
Our Services display some content that is not EquityLife?s. This content is the sole responsibility of the entity that makes it available. We may review content to determine whether it is illegal or violates our policies, and we may remove or refuse to display content that we reasonably believe violates our policies or the law. But that does not necessarily mean that we review content, so please don?t assume that we do.</p><p>

In connection with your use of the Services, we may send you service announcements, administrative messages, and other information. You may opt out of some of those communications.
</p>
Some of our Services are available on mobile devices. Do not use such Services in a way that distracts you and prevents you from obeying traffic or safety laws.
<p></p>
Your EquityLife Account
<p>
You may need a EquityLife Account in order to use some of our Services. You may create your own EquityLife Account, or your EquityLife Account may be assigned to you by an administrator, such as your employer or educational institution. If you are using a EquityLife Account assigned to you by an administrator, different or additional terms may apply and your administrator may be able to access or disable your account.
</p>
To protect your EquityLife Account, keep your password confidential. You are responsible for the activity that happens on or through your EquityLife Account. Try not to reuse your EquityLife Account password on third-party applications. If you learn of any unauthorized use of your password or EquityLife Account, follow these instructions.
<p>
Privacy and Copyright Protection
</p><p>
EquityLife's privacy policies explain how we treat your personal data and protect your privacy when you use our Services. By using our Services, you agree that EquityLife can use such data in accordance with our privacy policies.
</p><p>
We respond to notices of alleged copyright infringement and terminate accounts of repeat infringers according to the process set out in the U.S. Digital Millennium Copyright Act.
<p></p>
We provide information to help copyright holders manage their intellectual property online. If you think somebody is violating your copyrights and want to notify us, you can find information about submitting notices and EquityLife?s policy about responding to notices in our Help Center.
<p></p>
Your Content in our Services
</p><p>
Some of our Services allow you to upload, submit, store, send or receive content. You retain ownership of any intellectual property rights that you hold in that content. In short, what belongs to you stays yours.
</p><p>
When you upload, submit, store, send or receive content to or through our Services, you give EquityLife (and those we work with) a worldwide license to use, host, store, reproduce, modify, create derivative works (such as those resulting from translations, adaptations or other changes we make so that your content works better with our Services), communicate, publish, publicly perform, publicly display and distribute such content. The rights you grant in this license are for the limited purpose of operating, promoting, and improving our Services, and to develop new ones. This license continues even if you stop using our Services (for example, for a business listing you have added to Google Maps). Some Services may offer you ways to access and remove content that has been provided to that Service. Also, in some of our Services, there are terms or settings that narrow the scope of our use of the content submitted in those Services. Make sure you have the necessary rights to grant us this license for any content that you submit to our Services.
</p><p>
Our automated systems analyze your content (including emails) to provide you personally relevant product features, such as customized search results, tailored advertising, and spam and malware detection. This analysis occurs as the content is sent, received, and when it is stored.
</p><p>
If you have a EquityLife Account, we may display your Profile name, Profile photo, and actions you take on EquityLife or on third-party applications connected to your EquityLife Account (such as +1?s, reviews you write and comments you post) in our Services, including displaying in ads and other commercial contexts. We will respect the choices you make to limit sharing or visibility settings in your EquityLife Account. For example, you can choose your settings so your name and photo do not appear in an ad.
</p><p>
You can find more information about how EquityLife uses and stores content in the privacy policy or additional terms for particular Services. If you submit feedback or suggestions about our Services, we may use your feedback or suggestions without obligation to you.
</p><p>
About Software in our Services
</p><p>
When a Service requires or includes downloadable software, this software may update automatically on your device once a new version or feature is available. Some Services may let you adjust your automatic update settings.
</p><p>
EquityLife gives you a personal, worldwide, royalty-free, non-assignable and non-exclusive license to use the software provided to you by EquityLife as part of the Services. This license is for the sole purpose of enabling you to use and enjoy the benefit of the Services as provided by EquityLife, in the manner permitted by these terms. You may not copy, modify, distribute, sell, or lease any part of our Services or included software, nor may you reverse engineer or attempt to extract the source code of that software, unless laws prohibit those restrictions or you have our written permission.
</p><p>
Open source software is important to us. Some software used in our Services may be offered under an open source license that we will make available to you. There may be provisions in the open source license that expressly override some of these terms.
</p><p>
Modifying and Terminating our Services
</p><p>
We are constantly changing and improving our Services. We may add or remove functionalities or features, and we may suspend or stop a Service altogether.
</p><p>
You can stop using our Services at any time, although we?ll be sorry to see you go. EquityLife may also stop providing Services to you, or add or create new limits to our Services at any time.
</p><p>
We believe that you own your data and preserving your access to such data is important. If we discontinue a Service, where reasonably possible, we will give you reasonable advance notice and a chance to get information out of that Service.
</p><p>
Our Warranties and Disclaimers
</p><p>
We provide our Services using a commercially reasonable level of skill and care and we hope that you will enjoy using them. But there are certain things that we don?t promise about our Services.
</p><p>
OTHER THAN AS EXPRESSLY SET OUT IN THESE TERMS OR ADDITIONAL TERMS, NEITHER EQUITYLIFE NOR ITS SUPPLIERS OR DISTRIBUTORS MAKE ANY SPECIFIC PROMISES ABOUT THE SERVICES. FOR EXAMPLE, WE DON?T MAKE ANY COMMITMENTS ABOUT THE CONTENT WITHIN THE SERVICES, THE SPECIFIC FUNCTIONS OF THE SERVICES, OR THEIR RELIABILITY, AVAILABILITY, OR ABILITY TO MEET YOUR NEEDS. WE PROVIDE THE SERVICES ?AS IS?.
</p><p>
SOME JURISDICTIONS PROVIDE FOR CERTAIN WARRANTIES, LIKE THE IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, WE EXCLUDE ALL WARRANTIES.
</p><p>
Liability for our Services
</p><p>
WHEN PERMITTED BY LAW, EQUITYLIFE, AND EQUITYLIFE?S SUPPLIERS AND DISTRIBUTORS, WILL NOT BE RESPONSIBLE FOR LOST PROFITS, REVENUES, OR DATA, FINANCIAL LOSSES OR INDIRECT, SPECIAL, CONSEQUENTIAL, EXEMPLARY, OR PUNITIVE DAMAGES.
</p><p>
TO THE EXTENT PERMITTED BY LAW, THE TOTAL LIABILITY OF EQUITYLIFE, AND ITS SUPPLIERS AND DISTRIBUTORS, FOR ANY CLAIMS UNDER THESE TERMS, INCLUDING FOR ANY IMPLIED WARRANTIES, IS LIMITED TO THE AMOUNT YOU PAID US TO USE THE SERVICES (OR, IF WE CHOOSE, TO SUPPLYING YOU THE SERVICES AGAIN).
</p><p>
IN ALL CASES, EQUITYLIFE, AND ITS SUPPLIERS AND DISTRIBUTORS, WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE THAT IS NOT REASONABLY FORESEEABLE.

</p>
<p>

Business uses of our Services</p>
</p><p>
If you are using our Services on behalf of a business, that business accepts these terms. It will hold harmless and indemnify EquityLife and its affiliates, officers, agents, and employees from any claim, suit or action arising from or related to the use of the Services or violation of these terms, including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys? fees.
</p><p>
About these Terms
</p><p>
We may modify these terms or any additional terms that apply to a Service to, for example, reflect changes to the law or changes to our Services. You should look at the terms regularly. We?ll post notice of modifications to these terms on this page. We?ll post notice of modified additional terms in the applicable Service. Changes will not apply retroactively and will become effective no sooner than fourteen days after they are posted. However, changes addressing new functions for a Service or changes made for legal reasons will be effective immediately. If you do not agree to the modified terms for a Service, you should discontinue your use of that Service.
</p><p>
If there is a conflict between these terms and the additional terms, the additional terms will control for that conflict.
</p><p>
These terms control the relationship between EquityLife and you. They do not create any third party beneficiary rights.
</p><p>
If you do not comply with these terms, and we don?t take action right away, this doesn?t mean that we are giving up any rights that we may have (such as taking action in the future).
</p><p>
If it turns out that a particular term is not enforceable, this will not affect any other terms.
</p><p>
The laws of Washington, DC U.S.A., excluding District of Columbia's conflict of laws rules, will apply to any disputes arising out of or relating to these terms or the Services. All claims arising out of or relating to these terms or the Services will be litigated exclusively in the federal or state courts of Washington, DC USA, and you and EquityLife consent to personal jurisdiction in those courts.
</p><p>
For information about how to contact EquityLife, please visit our contact page.
</p>

		</div>






				<div id="privacy" style="margin-bottom: 10%;">
<h4 style="color: black">

Equity Life Privacy Policy</h4><p>

<p>
Last Updated July 2014</p><p>
Copyright Equity Life</p><p>
Welcome to Equity Life. Please read our privacy policy carefully to understand how we will use the information you provide while using sites associated with Equity Life, i.e., www.equitylife.org.This policy may change from time to time, so please check the website each time you use it for the most current information.</p><p>
This privacy policy contains information on the following subjects:</p><p>
What Information we Collect from Clients</p><p>
How We Collect Information</p><p>
Use of Cookies and Tracking User Traffic</p><p>
The General Purposes for which we Use and Disclose the Information</p><p>
How to Opt-Out of Certain Uses of the Information we collect</p><p>
How We Protect Your Information</p><p>
When used in this Privacy Policy, the terms below have the following meanings:</p><p>
"Personal Contact Information" means basic account and profile information entered by or on behalf of a client. This type of information is used to make contact and it includes the following type of information.</p><p>
- First and Last name (middle initial and suffix, if applicable);</p><p>
- Gender</p><p>
- Home or Mobile Telephone Number</p><p>
- E-mail Address</p><p>
- Current US Mail Address</p><p>
- Credit Report Information</p><p>
"Sensitive Information"</p><p>
As a member, Equity Life may also collect the following information (in addition to basic contact information). This information is collected in order to carry out Equity Life legitimate business objectives.</p><p>
- Social Security Numbers</p><p>
- Credit or Debit Card Numbers, along with expiration date (if used as a form of payment)</p><p>
- Drivers' License Numbers, along with State of issue and address on license;</p><p>
- Bank Account Numbers (if used as form of payment);</p><p>
- Financial Account Numbers (for purposes of making disputes and interventions)</p><p>
- Date of Birth</p><p>
"Website Information" means information collected when individuals browse the www.equitylife.org website, such as date and time of visit, pages viewed, time spent on the site, and websites visited before viewing Equity Life site. This information cannot be traceable to any specific person.</p><p>
What Information We Collect From Clients</p><p>
When you visit equitylife.org (Member Site), you may provide us with three (3) different types of information: (a) personal contact information that you knowingly choose to disclose that is collected on an individual basis; (b) sensitive information; and (c) website use information collected on an aggregate basis by individuals browsing our website.</p><p>
How We Collect Information</p><p>
Personal Contact Information: We collect this information in order to contact you about your case or other relevant information from Equity Life.</p><p>
Sensitive Information: This information is only used for legitimate business purposes and is only shared with third parties to the extent necessary to fulfill those purposes. Sensitive information is encrypted before it is shared with any third party.
</p><p>

Website Information:</p><p>
- www.equitylife.org  When you browse this site, you are able to do so anonymously. Generally, we do not collect personal information when you browse this site - not even your email address. Your browser, however,  automatically tells us the type of computer and operating system you are using.</p><p>
- You only can access this site if you are a member of Equity Life.  In order to permit a user to access this site and protect and secure the privacy and confidentiality of its clients, Equity Life needs to track usage by client case number. Therefore, your use of the site constitutes consent to collection and storage of personally identifying information.
Use of Cookies and Tracking User Traffic.</p><p>
Some pages on this site may use "cookies," small files that the site places on your hard drive for identification purposes. A cookie file can contain information such as a user ID to track the pages visited. These files are used for site registration and customization the next time you visit us.</p><p>
Some parts of the site may also use cookies to track user traffic patterns. Equity Life does this in order to determine the usefulness of our Web site information to our users and to see how effective our navigational structure is in helping users reach that information. Please note that cookies cannot read data off of your hard drive. Your Web browser may allow you to be notified when you are receiving a cookie, giving you the choice to accept it or not. If you prefer not to receive cookies while browsing our Web site, you can set your browser to warn you before accepting cookies and refuse the cookie when your browser alerts you to its presence. You can also refuse all cookies by turning them off in your browser, By not accepting cookies, some pages may not fully function and you may not be able to access certain information on this site.
In some of our emails to you, Equity Life uses a "click-through URL." When you click one of these URLs, you pass through our web server before arriving at the web site that is your destination. We track "click-through" to help Equity Life determine your interest in particular topics and measure the effectiveness of our customer communications.</p><p>
Equity Life may use third-party advertising companies, including Google, to serve ads on its behalf. These companies may employ cookies and action tags (also known as single pixel gifs or web beacons) to measure advertising effectiveness and to serve ads based on a user?s prior visits to Equity Life's website. Equity Life uses Analytics data and the DoubleClick cookie to serve ads based on a user?s prior visits to the Equity Life website. Users may opt out of Google?s use of cookies, including the Double click cookie, by visiting the Google advertising opt out page. Users may opt out of Google Analytics by visiting the Google Analytics opt out page. Users may opt out of a third party advertising company's use of cookies by visiting the Network Advertising Initiative opt out page.
The General Purposes for Which We Use and Disclose Information</p><p>
How Equity Life Uses the Information</p><p>
Personal Contact Information: This type of information is used to correspond with Equity Life and is not shared unless the client has given permission in the Equity Life engagement agreement or doing so is necessary to provide the client needed legal services and fulfill Equity Life's legitimate business purposes.</p><p>
Sensitive Information: Sensitive information is used by Equity Life to provide enrolled clients with the services requested.
To save you time and make our web services even easier to use, some areas of the Equity Life website allow you to create a "Member ID" using your personal information. The system saves your information and assigns you a unique Equity Life Member ID. Next time you log into the client section of our web site you can simply enter your Member ID and password; the system will automatically look up the information it needs.
Under What Circumstances Equity Life Shares Your Information</p><p>
Personal Contact Information: Because the ability to communicate with our members during any part of the credit recovery process is absolutely essential, your personal contact information may be shared with other Equity Life employees. They will protect your personal information in accordance with the Equity Life Customer Privacy Policy.  Simply put, Equity Life?s employeeswill not share your personal contact information with other companies unless you specify otherwise. Equity Life works with other companies that help us provide Equity Life services to you, and we may provide your personal information to these companies. For example, should we need to forward written documents to you we give shipping companies this information so they can deliver more efficiently. The information they receive is for shipping and delivery purposes only, and we require that the companies safeguard your personal information in accordance with Equity Life policies.</p><p>
In addition, Equity Life uses your personal contact information to keep you up to date on the latest service announcements, feature updates, special offers, and other information we think you would like to hear about. From time to time, we may also use your personal information in order to contact you regarding participation in a market research survey, so that we can gauge Members? satisfaction and improve our services further.</p><p>
Sensitive Information: There may be some circumstances in which Equity Life shares certain sensitive information with a third-party vendor. However, when this occurs, the information is encrypted and Equity Life assures that it is handled in a manner consistent with this Privacy Policy and relevant laws. Sensitive information is only given to third party vendors when necessary to carry out the legitimate business objectives for which you retained the Firm. The circumstances under which sensitive information is shared with third parties include disputes and interventions to credit bureaus and creditors as well as processing of payments.
Website Information: This information is collected in order to allow Equity Life to improve its Web site and better serve its clients. This information allows Equity Life to design and arrange Web pages in the most user-friendly manner and to continually improve its Web site to better meet the needs of its clients and prospective clients.</p><p>
How to Opt Out of Certain Uses of the Information We Collect</p><p>
If you wish to opt out of any e-mail services offered by Equity Life, please click here. However, if you opt out of these services, Equity Life will no longer be able to provide you legal services and it will need to close your case.
Additionally, Equity Life may use third-party advertising companies, including Google, to serve ads on its behalf. These companies may employ cookies and action tags (also known as single pixel gifs or web beacons) to measure advertising effectiveness and to serve ads based on a users prior visits to Equity Life's website. Equity Life uses Analytics data and the DoubleClick cookie to serve ads based on a users prior visits to Equity Life website. Users may opt out of Googles use of cookies, including the Doubleclick cookie, by visiting the Google advertising opt out page. Users may opt out of Google Analytics by visiting the Google Analytics opt out page. Users may opt out of a third party advertising company?s use of cookies by visiting the Network Advertising Initiative opt out page. Any information that these third parties collect via cookies and action tags is completely anonymous.</p><p>
How We Protect Your Information</p><p>
Equity Life safeguards the security of the data you send with physical, electronic, and managerial procedures. We urge you to take every precaution to protect your personal data when you are on the Internet. Change your passwords often, use a combination of letters and numbers, and make sure you use a secure browser.</p><p>
- Application of Encryption Technologies Equity Life's web site uses industry-standard Secure Sockets Layer (SSL) encryption on all web pages where sensitive personal information, including social security numbers, is collected. To sign up for service, or to log into the client section of the web site, you must use an SSL-enabled browser. This protects the confidentiality of your personal and credit card or checking account information while it is transmitted over the Internet.</p><p>
- Evaluation of Information Protection Practices-Periodicallyour operations and business practices are reviewed for compliance with corporate policies and procedures governing the security, confidentiality and quality of our information.</p><p>
- Employee Access, Training and Expectations -Equity Life's corporate values, ethical standards, policies and practices are committed to the protection of customer information. In general, Equity Life business practices limit employee access to confidential information, and limit the use and disclosure of such information to authorized persons, processes and transactions.</p><p>
- Legally Compelled Disclosure of Information-Equity Life may disclose information when legally compelled to do so, in other words, when we, in good faith, believe that the law requires it or for the protection of our legal rights. Privileged information about a client's case is shared only when required by a court order or with a client's express, written permission.</p><p>
- Sharing Information with Outside Parties-Equity Life may provide aggregate information about our site visitors and related Web site information to our affiliates, but this information will not include Sensitive Information.</p><p>
Our Company-Wide Commitment To Privacy-To make sure your personal information remains confidential, Equity Life communicates these privacy guidelines to every Equity Life employee, staff member, and third-party vendor.</p><p>
Protection of Minors-Equity Life does not knowingly solicit personal information from children or send them requests for personal information.
Third-Party Web Site Links-Equity Life's web site may contain links to other sites. Equity Life  does not share your personal information with those web sites and is not responsible for their privacy practices. We encourage you to learn about the privacy policies of those companies.
Changes to the Use of Personal Information-If Equity Life is going to use your personal information differently from that stated at the time of collection, we would try to contact you via email using the most recent information we have. If you have not given us permission to communicate with you, you will not be contacted, nor will we use your personal information in a new manner.</p><p>
Equity Life's Privacy Policy is subject to change at any time. We encourage you to review the privacy policy regularly for any changes.

</p>		</div>






		<div id="faq" style="margin-bottom: 10%;">
<h4 style="color: black">FAQ</h4><p>How much does membership in Equity Life cost? Our membership is a one-time fee of 499.95
Why? Our competition makes money off of monthly fees. They work slowly to get money out of you over a long period of time. We do everything we can as fast as possible so your score will improve ASAP.</p><p>
Why is it better?  Our system is based in a solid foundation of financial and credit education, coupled with a Consultant who guides you through the process of improving your financial situation, thereby also improving your credit situation.  </p><p>
How does Equity Life's guarantee work? After 365 days of service, you have the option to receive a full refund if you are not satisfied. This refund will be sent to you within 48 hours.</p><p>
Can Your Current Credit profile improve?</p><p>
Absolutely.  First, we thoroughly review your current credit situation.  Next, we develop a step by step action plan to assist you in your path to financial education and improving your credit profile.  Our Consultants are there to help explain the reasons behind the action plan, to educate you so that you will become a more financially savvy consumer.  Our Consultants are there also to help keep you on track to achieve your financial goals.  . Once you are a member our Consultant  will help you to improve your credit profile or you get your money back. Were here 24/7 to look at your file and let you know where our team is in getting your score up.
Process Questions (How long will everything take?)
Just one specific Consultant  will be working on your file. Whenever there is any action, we will email you and update your file.</p><p>
You can call anytime, 24/7 for us to let you know where we are in the following process:</p><p>
1. Pull and analyze credit report same day as sign up-payment.</p><p>

2. 15 minute consultation within 72 hours to present you opportunities to improve your financial education, improve your financial situation and improve your credit profile..</p><p>
3. 30 days.</p><p>
a. All inaccurate negative items on credit report disputed.</p><p>
b. New accounts are sought.</p><p>
4. 45 days.</p><p>
a. We'll know what positive items we can add.</p><p>
b. Youll have a new credit score.</p><p>
c. Satisfaction & Action Plan</p><p>
Company Questions</p><p>
Our company has been helping people improve their financial lifestyle for the past four years, working with thousands of people across United States. Our office is in 1701 Pennsylvania Ave NW Washington, DC 20006. Can I repair my own credit? Yes You Can.
</p>

</div>
	</div></div>

<div id="light2" class="white_content" align="center">

 	<h3 style="padding-top: 5%; color: black">Search</h3>
			<script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>





	<div class="footer-section">
		<div class="container">
		<p class="footer-text">Equity Life 2014. All Rights Reserved</p>


				<li><img src="../images/googleplay.png"></li>
				<li><img src="../images/apple.png"></li>




	<ul class="social-box">



				<li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
				<li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
				<li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


		<li><a href="#" class="social-icon">&#62217;</a></li>
		<li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
		<li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
		<li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>

		</ul>


		</div>
	</div>





</body>

</html>
