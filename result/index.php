<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=550, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="css/demo.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="src/js/jquery.flipster.js"></script>

</head>
<body>

<style>

	h3{
		color: white;
	}

	label{
		font-family: 'Mission Gothic', sans-serif;
		float: left;
	}

	span{
		color: white!important;
	}

	@font-face {
		font-family: 'Entypo-social'!important;
		src: url('fonts/entypo-social.ttf')!important;
	}

	body{
		background-color: black;
	}

	.flipster .flipster-nav{
		background-color: #3f3f3f!important;
		border-bottom: solid 2px #AF0014!important;
	}

	#answers{
		margin: 0 auto;
		width: 600px;
	}

	input[type="radio"]{
		float: left;
	}

	.footer-text{
		margin-left: 55px;
	}

	.flip-past{
		opacity: 0.3;
	}

	.flip-future{
		opacity: 0.3;
	}


</style>

<div class="top-bar">
	<div class="container">
		<a href="http://equitylife.org/welcome"><img src="../img/logo.png"></a>
		<a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'"></a>
		<nav>
			<a href="#" id="menu-icon"></a>
			<ul>
			<li><a href="http://equitylife.org/apply" class="su-li">Apply Now</a></li>
			<li><a href="http://equitylife.org/who-we-are">Who We Are</a></li>
			<li><a href="http://equitylife.org/our-focus">Our Focus</a></li>
			<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
			<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
			<li><a href="http://equitylife.org/citizenship">Citizenship</a></li>
			<li><a href="http://equitylife.org/lifestyle-categories">Lifestyle</a></li>
			<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>			</ul>
		</nav>
		<a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
	</div>
</div>


<header id="Main-Header">

</header>
<div id="Main-Content">
	<div class="Container">

<!-- Flipster List -->
	<center>
		<div id="Coverflow">
		  <p>.</p>
		  <p>.</p>
		  <p>.</p>
		  <p>.</p>
		  <div style="padding:0 auto; width: 500px">
		  	<h3>Quiz Results</h3>
		  	<p> <span>Your score:</span><span id="score"></span>  </p>
		  	<p> <span>Passing score:</span><span id="passing"></span>  </p>
		  	<p> <span>Elapsed time:</span><span id="time"></span>  </p>
		  </div>
		</div>

		<p>.</p>
		  <div style="padding:0 auto; width: 500px">
		  	<h3>Quiz Message</h3>
		  	<p><span id="message"></span></p>
		  	<br />
		  	<br />
		  	<br />
		  	<br />

        <form name="input" action="/register" method="get">
          <input type="submit" value="Register" class="button2" />
        </form>

		  	<br />
		  	<br />
		  	<br />
		  	<br />
		  	<br />
		  	<br />
		  	<br />
		  </div>
		</div>

	</center>
<!-- End Flipster List -->



	</div>
</div>






<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('5432-494-10-4139');/*]]>*/</script><noscript><a href="https://www.olark.com/site/5432-494-10-4139/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->






<script>

	function gup( name )
	{
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( window.location.href );
	  if( results == null )
	    return null;
	  else
	    return results[1];
	}


	$(function(){

		var total = gup("k");
		var tiempo = gup("m") + ":" + gup("s") + ":" + gup("ms");
		var score = total * 100 / 22;
		score = Math.round( score * 10 ) / 10
		$("#score").text(score + " (" + total + " de 22 )"  );
		$("#time").text(tiempo);
		$("#passing").text("16 (70%)");

		var message = "";
		if(total <= 16)
			message = "You failed.";
		else
			message = "You do it. Congratulations!";

		$("#message").text(message);

	});

</script>
</body>
</html>
