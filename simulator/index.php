<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

  <!-- CHIEF CREATIVE 2014
  ================================================== -->
  <meta charset="utf-8">
  <title>Corporate - Equity Life</title>
  <meta name="description" content="Chief Creative Labs">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


  <link rel="stylesheet" href="../stylesheets/grid.css">
  <link rel="stylesheet" href="../stylesheets/media.css">
  <link rel="stylesheet" href="../stylesheets/styles.css">
  <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="../js/Chart.js"></script>

  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="../images/favicon.ico">

</head>
<body>

<style>
.i2{
  width: 210px!important;
  border: solid black 2px!important;
  float: left!important;
  margin-right: 30px!important;
}

.i1{
  width: 450px!important;
  border: solid black 2px!important;
  float: left!important;
}

.i3 {
  width: 140px!important;
  border: solid black 2px!important;
  float: left!important;
  margin-right: 15px!important;
}
.data{
  margin-top: 20px;
  padding: 32px;
  border: solid rgb(167, 167, 167) 3px;
  font-size: 15px;
}
.iqscore{
  height: 300px;
  background-image: none!important;
  background-color: black!important;
  background-size: cover;
  background-position-y: -35px;
}
</style>

<script>

  $(function(){

    var lineData = {
      labels: ["", "", "", "", "", "", ""],
      datasets: [
          {
              label: "",
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: [645,708,731,744,762]
          },
      ]
    };
    var ctx = document.getElementById("lineChart").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData,{bezierCurve:false});

    var donutData = [
    {
        value: 60,
        color:"#514848",
        highlight: "#bdbdbd",
        label: "Inquires"
    },
    {
        value: 45,
        color: "#585555",
        highlight: "#bdbdbd",
        label: "Average Ages of Accounts"
    },
    {
        value: 20,
        color: "#6f6f6f",
        highlight: "#bdbdbd",
        label: "Age of Credit History"
    },
    {
        value: 65,
        color: "#242323",
        highlight: "#bdbdbd",
        label: "No Inshallment Account"
    },
    {
        value: 180,
        color: "#3e3c3c",
        highlight: "#bdbdbd",
        label: "Age of Credit History"
    }];
    var dctx = document.getElementById("donutChart").getContext("2d");
    var myPieChart = new Chart(dctx).Pie(donutData);






  })

</script>


<style>
.container .nine.columns{
  width: 470px;
}

.column-l{
  width: 20%;
  float: left;
  padding-top:40px;

}

.column-r {
  width: 60%;
  float: left;
  background-color: white;
  padding-left: 15px;
  height: 100%;
  padding-right: 15px;
}

.column-l a {
  color: black;
  font-family: futura;
  text-transform: none;
  font-size: 17px;
  display: block;
}

body{
  background: #E7E7E7;
}

h3{
  color: black;
}
</style>

<div class="top-bar">
<div class="container" >

  <a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>


  <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
      <nav>

    <a href="#" id="menu-icon"></a>
        <ul>
        <li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
        <li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>
        <li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
        <li><a href="http://equitylife.org/innovation">Innovation</a></li>
        <li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
        <li><a href="http://equitylife.org/what-we-love">What We Love</a></li>

              </ul>

          </nav>






        <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>




</div>

</div>



<div class="iqscore" style="height: 400px;">
<div class="container" >
  <div class="" style="padding-top: 15%;" align="center">
    <h1>SIMULATOR</h1>
  </div>




</div></div>
<div class="container" style="height: 75%;">

  <div class="column-l">

    <a href="../my-alerts">Account Summary</a><br />
    <a href="../credit-report/">Credit Report</a><br />
    <a href="../score-manager/">Score Manager</a><br />
    <a href="../simulator">Simulator</a><br />
    <a href="../my-profile/">Profile</a><br />

  </div>
  <div class="column-r">

<table  style="margin-top: 50px">
  <tr>
    <td style="width: 151px;">
REVOLVING
    </td>
    <td style="width: 151px;">INSTALLEMENT
    </td>
    <td style="width: 151px;">COLLECTIONS </td>
    <td style="width: 151px;">PUBLIC RECORDS</td>
    <td style="width: 151px;">OTHERS</td>
    <td style="width: 151px;">INQUIRIES </td>
  </tr>
</table>

    <table class="data" width="100%;" style="margin-top: 20px">
                <tbody>
                  <tr class="applloyGridAltItem">

                    <td colspan="5" align="center"  style="font-weight: bold;">
                        REVOLVING
                    </td>

                </tr>

            <tr class="AccountTrig">

                <td align="center" >
                    CAP ONE
                </td>
                <td align="center" >
                    Account #:...
                </td>
                <td align="center" >
                    Balance: $774.00
                </td>
                <td align="center" >
                    <input type="button" value="REMOVE" />
                </td>
                <td align="center"  style="text-align: right;">
                    <input type="button" value="MODIFY" />
                </td>
            </tr>
            <tr class="AccountTrig">

                <td align="center" >
                    CAP ONE
                </td>
                <td align="center" >
                    Account #:...
                </td>
                <td align="center" >
                    Balance: $774.00
                </td>
                <td align="center" >
                    <input type="button" value="REMOVE" />
                </td>
                <td align="center"  style="text-align: right;">
                    <input type="button" value="MODIFY" />
                </td>
            </tr>
            <tr class="AccountTrig">

                <td align="center" >
                    DSNB MACYS
                </td>
                <td align="center" >
                    NAVY FCU
                </td>
                <td align="center" >
                    Balance: $774.00
                </td>
                <td align="center" >
                    <input type="button" value="REMOVE" />
                </td>
                <td align="center"  style="text-align: right;">
                    <input type="button" value="MODIFY" />
                </td>
            </tr>



    </tbody></table>












  </div>

</div>


<div id="light2" class="white_content" align="center">

   <h3 style="padding-top: 5%; color: black">Search</h3>
      <script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>



    <div id="light" class="black_content" align="center">



<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>

  </div>



  <div class="footer-section">
    <div class="container">
    <p class="footer-text">Equity Life 2014. All Rights Reserved</p>


        <li><img src="../images/googleplay.png"></li>
        <li><img src="../images/apple.png"></li>




  <ul class="social-box">



        <li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
        <li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
        <li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


    <li><a href="#" class="social-icon">&#62217;</a></li>
    <li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
    <li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
    <li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>

    </ul>


    </div>
  </div>





</body>

</html>
