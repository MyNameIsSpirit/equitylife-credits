<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- CHIEF CREATIVE 2014
  ================================================== -->
	<meta charset="utf-8">
	<title>Terms of Referral Program - Equity Life</title>
	<meta name="description" content="Chief Creative Labs">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<link rel="stylesheet" href="../stylesheets/grid.css">
	<link rel="stylesheet" href="../stylesheets/media.css">
	<link rel="stylesheet" href="../stylesheets/styles.css">
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.ico">

</head>
<body>

	
<div class="top-bar">
<div class="container">

	<a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>
	

	<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
				<nav>
		
		<a href="#" id="menu-icon"></a>
				<ul>
				<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
				<li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>	
				<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>				
				<li><a href="http://equitylife.org/innovation">Innovation</a></li>		
				<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
				<li><a href="http://equitylife.org/what-we-love">What We Love</a></li>
				
							</ul>
			
					</nav>
		
		
	

		
				<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
			
		
		
		
</div>

</div>

<div class="left-section2">
	<div class="legal-box" align="left;">

	<a href="#definitions" class="legal-links">Definitions</a><br>
	<a href="#program-overview" class="legal-links">Program Overview</a><br>
	<a href="#distribution" class="legal-links">Distribution</a><br>
	<a href="#compliance" class="legal-links">Compliance</a><br>
	<a href="#termination" class="legal-links">Termination and<br> Modifications</a><br>
	<a href="#brand-features" class="legal-links">Brand Features</a><br>
	<a href="#payment" class="legal-links">Payment</a><br>
	<a href="#restrictions" class="legal-links">Restrictions</a><br>
	<a href="#term-termination" class="legal-links">Term: Termination</a><br>
	<a href="#confidentiality" class="legal-links">Confidentiality</a><br>
	<a href="#indemnification" class="legal-links">Indemnification</a><br>
	<a href="#disclaimers" class="legal-links">Disclaimers</a><br>
	<a href="#representations" class="legal-links">Representations</a><br>
	<a href="#miscellaneous" class="legal-links">Miscellaneous</a><br>
	</div></div>
		

		<div class="s-box">
		<div style="margin-bottom: 10%;">
		
		<h4 style="color: black">Terms : EQUITY LIFE Referral Program</h4><p>
This Equity Life Referral Program Agreement ("Agreement") is entered into by Equity Life Insurance LLC. ("EquityLife") and the referring party executing this Agreement ("Participant"). This Agreement governs Participant?s participation in the Equity Life for Membership Referral Program described herein (the "Program"). You represent and warrant that: (i) you have full legal authority to enter into this Agreement; (ii) you have read and understand the Agreement, and (iii) you agree to the terms of this Agreement. If you do not have legal authority to enter into this Agreement or do not agree to these terms, please do not click to accept this Agreement below.</p><p>
This Agreement is effective between Participant and EquityLife as of the date Participant electronically accepts this Agreement.
</p>
		
		</div>
		
		
		
		
		
		<div id="definitions" style="margin-bottom: 10%;">
<h4 style="color: black">

Definitions</h4><p>
"Custom URL" means the unique URL to be provided by Equity Life to Participant and which will correlate Eligible Referrals submitted by Participant to Participant?s Program account.</p><p>
"Eligible Referral" means an eligible net new customer lead for the direct online purchase of the Services directly from EquityLife, excluding Ineligible Referrals.</p><p>
"Equity Life Credit Expert Program" or "Services" means the Education Core Services generally sold by Equity Life as EquityLife Credit Expert Program and further described here: http://www.equitylife.org/our-focus as such URL, and Services description, may be updated by Equity Life from time to time.</p><p>
"Incentives" means redeemable coupons, or other incentives provided by EquityLife to Participant for distribution to Eligible Referrals in connection with their purchase of Services.</p><p>
"Ineligible Referrals" means government-controlled entities and any of their employees, political parties and candidates, Services customers of resellers, and any other individuals or entities already in Equity  life?s.</p><p>
"Participant" means a member of the Program with a primary place of business or residence in the Territory that is a business, entity, or other individual with a valid tax ID number but may not include (i) government-controlled entities and any of their employees, (ii) political parties and candidates, and (iii) representatives, agents, or employees of EquityLife.</p><p>
"Program Guide" means any set of applicable Program terms in addition to this Agreement that EquityLife may make available to Participant via the Resource Portal and that will govern Participant's continued participation in the Program.</p><p>
"Referral" is a new customer lead for Services submitted by Participant, to EquityLife.</p><p>
"Referral Fees" means the fees to be paid by EquityLife to Participant for Eligible Referrals that result in a Valid Transaction.</p><p>
"Resource Portal" means the website provided by EquityLife to Participant containing Program resource tools and information.</p><p>
"Territory" is limited to the geographic regions of North America (defined as the United States) and Canada.</p><p>
"Valid Transaction" is a closed sale for Services originating through a Participant?s Custom URL and which is made online directly between an Eligible Referral and EquityLife in compliance with the requirements for payment of Referral Fees.
		</p>
		</div>
		
		
		
		
		
			
		
		
		
			<div id="program-overview" style="margin-bottom: 10%;">
<h4 style="color: black">

Program Overview.</h4><p> In compliance with this Agreement Participant will promote the Services only to Eligible Referrals whose principal place of business is located in the Territory. Participant will not market the Services to any Ineligible Customers. EquityLife will provide Participant with a Custom URL for the purpose of correlating any Eligible Referrals resulting in a Valid Transaction under this Agreement. Equity Life may issue Incentives to Participant in accordance with the restrictions and guidelines in this Agreement. As applicable, and subject to the requirements herein, Participant may include on its website and in its marketing materials for the Services the Custom URL provided by EquityLife. A Valid Transaction must always originate from Participant?s Email to qualify for Referral Fees.</p>
Equity Life may elect to create an applicable Program Guide which may include updates regarding applicable Referral Fees, Incentives and other relevant Program details. The terms of any such Program Guide are made part of this Agreement and will control over any conflicting term in the body of this Agreement


		</div>

		
		
		
		
		
				<div id="distribution" style="margin-bottom: 10%;">
<h4 style="color: black">

Distribution of Custom URL and Incentives.</h4><p> Participant will use best efforts to distribute the Custom URL and Incentives: (a) only to Eligible Referrals; and (b) only after Equity Life has approved the distribution by providing Participant with the Custom URL and any applicable Incentives. All marketing materials (including without limitation the text of email distributions, if any) must be (i) strictly consistent with any Program instructions provided by EquityLife, (ii) compliant with the terms and conditions of this Agreement and all applicable marketing, privacy and data protection laws and governmental regulations. Any e-mail distribution must: (x) offer recipients the ability to opt-out of future Participant communications; and (y) contain the EquityLife governing terms and conditions for any Incentives when offered. At Equity Life?s request, Participant will block distribution of Custom URL and Incentives to parties as EquityLife designates in its sole discretion, consistent with applicable laws.		</p>
		</div>

		
		
		
		
		
		<div id="compliance" style="margin-bottom: 10%;">
<h4 style="color: black">Compliance</h4><p> In addition to the other legal requirements in this Agreement, Participant expressly agrees to comply with the following:</p><p>
?	4.1. Anti-Bribery Laws and Reporting. Participant will comply with all applicable commercial and public anti-bribery laws ("Anti-Bribery Laws"), including the U.S. Foreign Corrupt Practices Act of 1977 and the UK Bribery Act of 2010, which prohibit corrupt offers of anything of value, either directly or indirectly to anyone, including government officials, to obtain or keep business or to secure any other improper commercial advantage. "Government officials" include any government employee; candidate for public office; and employee of government-owned or government-controlled companies, public international organizations, and political parties. Furthermore, Participant will not make any facilitation payments, which are payments to induce officials to perform routine functions they are otherwise obligated to perform. If Participant becomes aware of suspicious, illegal or fraudulent activity occurring in relation to this Agreement, Company will report the suspicious or fraudulent activity to EquityLife within 24 hours of identifying the suspicious or fraudulent activity to the following email alias .</p><p>
?	4.2 Export Control Laws. Partner will comply with all applicable export controls, including, but not limited to, the United States Department of Commerce's Export Administration Regulations and sanctions programs administered by the United States Treasury Department's Office of Foreign Assets Control.</p><p>
?	4.3 Acceptable Use Policy. Participant will not send, post, transmit or otherwise use any EquityLife provided content, including the EquityLife name or the Services, in connection with any materials, sites or otherwise that: (i) will generate or facilitate unsolicited bulk commercial emails; (ii) will, or otherwise encourage, the violation of the legal rights of others; (iii) is for an unlawful, invasive, infringing, defamatory, or fraudulent purpose; (iii) contains obscene or pornographic content.</p><p>
?	4.4 Certification. By submitting a Referral to EquityLife, Participant represents, warrants and certifies to EquityLife that each such submission will be: (i) reasonably made in good faith as an Eligible Referral; (ii) comply with all applicable laws and governmental regulations, including without limitation, as described above, and (iii) Participant is not otherwise restricted from providing the Referral to EquityLife or acting as a Participant in the Program.
<p>

</div>
		





		<div id="termination" style="margin-bottom: 10%;">
<h4 style="color: black">Termination and Modification of Terms</h4><p> EquityLife may at any time and in its sole discretion: (i) modify or update the terms of, including but not limited to, the Program, this Agreement, Incentives (and their governing terms and conditions) and will make such modified or updated terms available in writing, or (ii) revoke a Participant account, and/or terminate Participant?s participation in the Program in its entirety. If EquityLife provides Participant with an updated Custom URL, or Incentives (and applicable terms and conditions), Participant agrees will begin using, and will be subject to, such updated versions no later than 30 days after receiving them.
</p>
</div>
		
		
		
		
			<div id="brand-features" style="margin-bottom: 10%;">
<h4 style="color: black">Brand Features</h4> <p>Each party will own all right, title and interest to trade names, trademarks, service marks, logos and domain names it secures from time to time ("Brand Feature(s)"). Subject to the terms and conditions of the Agreement (including without limitation the following sentence), EquityLife grants to Participant a nonexclusive and non-sublicensable license during the Term to display EquityLife?s Brand Features only to the extent Brand Features are provided by EquityLife for use with the Program as indicated through the Resource Portal and solely for the limited purpose of promoting the Services consistent with this Agreement. Additionally, all use of EquityLife Brand Features is at EquityLIife?s sole discretion and subject to EquityLife?s then-current Brand Feature use guidelines currently located at http://www.Equitylife.org/permissions/guidelines.html, as such URL may be updated from time to time by EquityLife.</p><p>
Subject to the terms and conditions of the Agreement, Participant grants to EquityLife a nonexclusive and non-sublicensable license during the Term to display Participant?s Brand Features solely for the purpose of marketing the Program or as otherwise mutually agreed upon (email permitted).
</p>

</div>
			
		
		
		
		
				
			<div id="payment" style="margin-bottom: 10%;">
<h4 style="color: black">Payment</h4><p>
?	7.1 Referral Fees. EquityLife will pay Participant a one-time fee of $100 (a "Referral Fee") for each Eligible Referral?s end user that results in a Valid Transaction provided that in no event will EquityLife owe any payments to Participant for any single Eligible Referral.</p><p>?	7.2. Incentives. EquityLife may elect to provide Participant with Incentives to offer its Eligible Referrals from time to time. Use of any applicable Incentives will be subject to the governing terms and conditions as provided by EquityLife.</p><p>
?	7.3. Payment Process. In order for Participant to receive any Referral Fees participant must first complete the required referral payment form as required by EquityLife and maintain a valid banking account within the Territory. Equity Life will pay Participant the Referral Fees in the form of a direct deposit to the banking account as indicated by Participant via the Program registration process and completion of the required Referral payment form. Additional payment terms may need to be accepted by Participant for the deposit of Referrals Fees to Participant?s banking account. Referral fees will be issued to Participant on a monthly basis. All amounts referenced or payable under this Agreement are in U.S. Dollars unless EquityLife notifies otherwise.</p><p>
Participant is solely responsible for ensuring its account information is accurate and current. EquityLife will not be responsible for any payments not received due to Participant failing to provide accurate and complete account information for payment.</p>

</div>
		
		
		
				
			<div id="restrictions" style="margin-bottom: 10%;">
<h4 style="color: black">Restrictions</h4> <p>Participant will not, and will not allow any third party to: (a) "frame," minimize, remove, or otherwise inhibit, the full and complete display of any EquityLIfe web page; (b) cause any hyperlinks to web pages on the EquityLife web site to create a new browser window; or (c) otherwise display EquityLife web pages or Brand Features in a distorted or diluted fashion.</p>

</div>
		
		
		
		
		
					<div id="term-termination" style="margin-bottom: 10%;">
<h4 style="color: black">Term; Termination</h4><p>The Term will continue until terminated by either party as provided herein. Either party may terminate this Agreement immediately with or without cause upon written notice to the other party (email notice permitted). Upon any termination or expiration of this Agreement, all licenses and rights granted will terminate and each party will have no right to use the Brand Features of the other party. In the event of a termination, Participant will only receive Referral Fees that were earned in full for a Valid Transaction that occurred prior to the effective termination date. Termination notices to  EquityLife must be sent to the following email alias: close@equitylife.org</p>
</div>
		
		
		
		
		
		
		
							<div id="confidentiality" style="margin-bottom: 10%;">
<h4 style="color: black">Confidentiality</h4><p> Participant may not disclose the terms, conditions or existence of any non-public aspect of the Program to any third party, except to its professional advisors under a strict duty of confidentiality or as necessary to comply with law.
</p>

</div>
		
		
		
		
		
		
							<div id="indemnification" style="margin-bottom: 10%;">
<h4 style="color: black">Disclaimers; Limitation of Liability</h4><p> EACH PARTY DISCLAIMS ALL IMPLIED WARRANTIES, INCLUDING WITHOUT LIMITATION FOR NON-INFRINGEMENT, SATISFACTORY QUALITY, MERCHANT-ABILITY AND FITNESS FOR ANY PURPOSE. TO THE FULLEST EXTENT PERMITTED BY LAW, THE PROGRAM AND CUSTOM URL IS PROVIDED "AS IS" AND AT PARTICIPANT?S OPTION AND RISK AND GOOGLE DOES NOT GUARANTEE ANY RESULTS. EXCEPT FOR (I) BREACHES OF SECTION 4 (COMPLIANCE); (ii) BREACHES OF SECTION 6 (BRAND FEATURES); AND (iii) SECTION 12 (INDEMNIFICATION), TO THE FULL EXTENT PERMITTED BY LAW REGARDLESS OF THE THEORY OR TYPE OF CLAIM: (A) NO PARTY MAY BE HELD LIABLE UNDER THIS AGREEMENT OR ARISING OUT OF PERFORMANCE OF THIS AGREEMENT FOR ANY DAMAGES OTHER THAN DIRECT DAMAGES, EVEN IF THE PARTY IS AWARE OR SHOULD KNOW THAT SUCH DAMAGES ARE POSSIBLE AND EVEN IF DIRECT DAMAGES DO NOT SATISFY A REMEDY AND (B) NO PARTY MAY BE HELD LIABLE FOR DAMAGES UNDER THIS AGREEMENT IN THE AGGREGATE OF MORE THAN THE AMOUNT PAID BY GOOGLE TO PARTICIPANT UNDER THIS AGREEMENT IN THE 3 MONTHS BEFORE THE DATE OF THE ACTIVITY GIVING RISE TO THE FIRST CLAIM.
</p>

</div>




					<div id="disclaimers" style="margin-bottom: 10%;">
<h4 style="color: black">Indemnification</h4><p>Participant will defend, indemnify and hold harmless EquityLife, its agents, affiliates, and licensors from any third party claim or liability arising out of: (a) Participant?s participation in the Program; (b) Participant?s web site(s), Participant Brand Features and Google?s use of any Participant content (provided that such use complies with the requirements of the Agreement); and (c) Participant?s breach of the Agreement.</p>

</div>



				<div id="representations" style="margin-bottom: 10%;">
<h4 style="color: black">Representations and Warranties</h4> <p>Participant warrants that (a) Participant will use all information provided by EquityLife (including without limitation the EquityLife Brand Features) in a manner that complies with applicable law; (b) Participant will clearly and conspicuously display the text of the applicable terms and conditions for Incentives provided to Customers in accordance with this Agreement; and (c) Participant will conduct all activities in furtherance of this Agreement in accordance with applicable law.
</p>

</div>







	<div id="miscellaneous" style="margin-bottom: 10%;">
<h4 style="color: black">Miscellaneous</h4><p> All notices, unless otherwise stated herein, must be in writing and addressed to the attention of the other party?s Legal Department and primary point of contact. Notice will be deemed given when delivered (a) when verified by written receipt if sent by personal courier, overnight courier or mail or (b) when verified by automated receipt of electronic logs if sent by facsimile or email. Participant will not assign or otherwise transfer its rights or delegate its obligations under the Agreement, in whole or in part, and any attempt to do so will be null and void. The Agreement is not intended to benefit, nor shall it be deemed to give rise to, any rights in any third party. This Agreement is governed by California law, excluding California?s choice of law rules. ALL CLAIMS ARISING OUT OF OR RELATING TO THE SUBJECT MATTER OF THIS AGREEMENT WILL BE LITIGATED EXCLUSIVELY IN THE FEDERAL OR STATE COURTS OF Montgomery COUNTY, Maryland, USA, AND THE PARTIES CONSENT TO PERSONAL JURISDICTION IN THOSE COURTS. The parties are independent contractors and the Agreement does not create any agency, partnership, or joint venture. No party is liable for inadequate performance to the extent caused by a condition that was beyond its reasonable control. Failure to enforce any provision will not constitute a waiver. If any provision is found unenforceable, the balance of the provisions will remain in full force and effect. In the event of any termination or expiration of the Agreement, Sections 5, 9, 10, 11 and 12 shall survive. This Agreement is the parties? entire agreement relating to its subject and supersedes any prior or contemporaneous agreements on that subject. Any amendments must be in writing and executed by both parties (electronic form acceptable).
</p>

</div>
		
		
		</div>
	

<div id="light2" class="white_content" align="center">
	
 	<h3 style="padding-top: 5%; color: black">Search</h3>
			<script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
	
 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>
	
	

		<div id="light" class="black_content" align="center">
	
 

<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>
  
	</div>



	<div class="footer-section">
		<div class="container">
		<p class="footer-text">Equity Life 2014. All Rights Reserved</p>
	
							
				<li><img src="../images/googleplay.png"></li>
				<li><img src="../images/apple.png"></li>
			

				
		
	<ul class="social-box">
						
	
			
				<li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
				<li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
				<li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


		<li><a href="#" class="social-icon">&#62217;</a></li> 
		<li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
		<li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
		<li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>
		
		</ul>


		</div>
	</div>




	
</body>

</html>