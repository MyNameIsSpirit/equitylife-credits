<DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- CHIEF CREATIVE 2014
  ================================================== -->
	<meta charset="utf-8">
	<title>Corporate - Equity Life</title>
	<meta name="description" content="Chief Creative Labs">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<link rel="stylesheet" href="../stylesheets/grid.css">
	<link rel="stylesheet" href="../stylesheets/media.css">
	<link rel="stylesheet" href="../stylesheets/styles.css">
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700|Rokkitt:400,700' rel='stylesheet' type='text/css'>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="../images/favicon.ico">

</head>
<body>


<style>
.container .nine.columns{
	width: 470px;
}

</style>

<div class="top-bar">
<div class="container">

	<a href="http://equitylife.org/welcome"><img src="../images/logo.png"></a>


	<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" ></a>
			<nav>

		<a href="#" id="menu-icon"></a>
				<ul>
				<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>
				<li><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"">Member Center</a></li>
				<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
				<li><a href="http://equitylife.org/innovation">Innovation</a></li>
				<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
				<li><a href="http://equitylife.org/what-we-love">What We Love</a></li>

							</ul>

					</nav>






				<a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>




</div>

</div>



<div class="iqscore">
<div class="container">
	<div class="" style="padding-top: 10%;" align="center">
		<h1>IQ SCORE MANAGER</h1>
		<p class="large-text white"> Live a financially healthy lifestyle
               Simple. Fast. Effective.</p>
               <li><a href="http://equitylife.org/apply" class="button2">Apply Now</a></li>
<li><a href="http://equitylife.org/our-thinking" class="button2">Our Thinking</a></li>

			</div>




</div></div>
<div class="container">
<div class="margin" style="margin-left: 100px;margin-right: 100px;">
<h3>iQscore's powerful credit analytics show you the way.</h3>
<p>Is your credit history standing in the way of the lifestyle you want? iQualifier enables you to analyze your credit history with the same tools that lenders and credit agencies use. Then it takes you beyond that analysis demonstrating how different financial decisions may affect your credit in the months ahead. iQualifier can help you.
<br><br>
- Better understand your credit standing<br>
- Improve your financial decision making<br>
- Achieve your financial goals<br>
- Save money<br>
<br>
iQscore offers the most powerful credit management tools on the market today. Get the information and guidance you need to take control of your financial history and make your future happen.</p>






<h4>Your monthly iQ Score Manager subscription includes:</h4>
<p>Beyond saving money on purchases involving credit, a properly managed and well organized credit profile will mean that more companies will seek you out to provide credit and approve your requests for credit. When you have established your credibility as a consumer, a new world will open up for you.</p>
</div>




	<div class="nine columns right" style="margin-left: 100px;">
	<h4>The most powerful score simulator</h4>
	<img src="../images/increaseyourscore.png">
	<p>Say goodbye to guessing our Score Simulator enables you to make smart credit management decisions based on real information. Its' powerful technology analyzes your spending and payment habits, simulates how various actions you take affect your score, and shows what different decisions mean for you and your family.</p>
	</div>


	<div class="nine columns">
	<h4>A personalized action plan</h4>
	<img src="../images/actionplan.png">
	<p>Based on your history, iQualifier will devise an action plan that helps you reach your scoring goals. This plan provides ongoing guidance and automatically adjusts to accommodate changes that might occur in your credit history, such as new purchases or loan pay-offs.</p>

	</div>
</div><div class="container add-bottom">
<div class="margin">




	<div class="nine columns right" style="margin-left: 100px;">
	<h4>Monthly Credit reports and scores</h4>
	<img src="../images/personalreport.png">
	<p>Every month you?ll receive a new TransUnion credit report and credit score so you can track your progress. Nothing is more motivating than knowing you are working toward the future you want.</p>
	</div>


	<div class="nine columns" style="border-left: solid rgb(189, 189, 189) 1px;padding-left: 22px;">
	<h4>monthly email notifications</h4>
	<img src="../images/email.png">
	<p>We?ll send personalized, monthly email updates to help you stay on track.</p>
	<a href="http://equitylife.org/apply" class="button">Get Started</a>
	</div>

</div>
</div>


<div id="light2" class="white_content" align="center">

 	<h3 style="padding-top: 5%; color: black">Search</h3>
			<script>
  (function() {
    var cx = '010596511707209630062:dvmt2bo06y0';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>



		<div id="light" class="black_content" align="center">



<h3 style="padding-top: 5%;">WELCOME BACK</h3>

<ul>
<li ><input name="email" type="text"  placeholder="Email" class="black-border"></li><br>
<li><input name="password" type="password"  placeholder="Password" class="black-border"></li><br>
<li> <input type="submit" value="Log In" href="#" class="button"  ></li>
</ul><br>
<a href="#">Forgot Password</a><br>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" >Close</a></div>

 <div id="fade" class="black_overlay"></div>

	</div>



	<div class="footer-section">
		<div class="container">
		<p class="footer-text">Equity Life 2014. All Rights Reserved</p>


				<li><img src="../images/googleplay.png"></li>
				<li><img src="../images/apple.png"></li>




	<ul class="social-box">



				<li><a href="http://equitylife.org/corporate" class="footer-links">Corporate</a></li>
				<li><a href="http://equitylife.org/legal-notices" class="footer-links">Legal Notices</a></li>
				<li><a href="http://equitylife.org/support" class="footer-links">Help & Support</a></li>


		<li><a href="#" class="social-icon">&#62217;</a></li>
		<li><a href="https://plus.google.com/115424875365049224360/about" class="social-icon">&#62223;</a></li>
		<li><a href="https://www.linkedin.com/company/1652152?trk=prof-0-ovw-curr_pos" class="social-icon">&#62232;</a></li>
		<li><a href="http://instagram.com/equitylife" class="social-icon">&#62253;</a></li>

		</ul>


		</div>
	</div>





</body>

</html>
