<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=550, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../css/demo.css">

    <!-- bxSlider CSS file -->
    <link href="../css/jquery.bxslider.css" rel="stylesheet" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="..//js/jquery.bxslider.min.js"></script>

</head>
<body>

<style>

	nav li a:hover{
		color: rgb(0, 162, 224)!important;
	}

	label{
		font-family: 'Furuta', sans-serif;
		float: left;
	}

	@font-face {
		font-family: 'Entypo-social'!important;
		src: url('fonts/entypo-social.ttf')!important;
	}

	body{
		background-color: white;
	}

	.flipster .flipster-nav{
		background-color: #3f3f3f!important;
		border-bottom: solid 2px #AF0014!important;
	}

	#answers{
		margin: 0 auto;
		width: 400px;
	}

	input[type="radio"]{
		float: left;
	}

	.footer-text{
		margin-left: 55px;
	}

	.flip-past{
		opacity: 0.3;
	}

	.flip-future{
		opacity: 0.3;
	}

  .flipster .flipster-active .flipster-coverflow{
    padding-bottom: 0px;
  }


</style>




<div class="top-bar">
	<div class="container">
		<a style="margin-left: 60px;" href="http://equitylife.org/welcome"><img src="http://equitylife.org/images/logo-full.png"></a>
    <center style="position: absolute;top: 21px;margin: 0 auto;right: 320px;">
      <h3 style="font-size: 18px;line-height: 15px;color:white;"><a href="/register" style="color:black;"> SKIP TEST >>></a>  </h3>
    </center>
		<a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'"></a>
		<nav>
			<a href="#" id="menu-icon"></a>
			<ul>
			<li><a href="http://equitylife.org/apply" class="su-li">Apply Now</a></li>
			<li><a href="http://equitylife.org/who-we-are">Who We Are</a></li>
			<li><a href="http://equitylife.org/our-focus">Our Focus</a></li>
			<li><a href="http://equitylife.org/our-thinking">Our Thinking</a></li>
			<li><a href="http://equitylife.org/credit-insight">Credit Insight</a></li>
			<li><a href="http://equitylife.org/citizenship">Citizenship</a></li>
			<li><a href="http://equitylife.org/lifestyle-categories">Lifestyle</a></li>
			<li><a href="http://equitylife.org/iqscore">Iqscore</a></li>			</ul>
		</nav>
		<a href="javascript:void(0)" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="search-icon search-placement"></a>
	</div>
</div>




<div id="Main-Content">
	<div class="Container">
<!-- Flipster List -->
		<div id="Coverflow">
		  <ul class="bxslider" style="padding-top: 100px;">
		  	<li id="Coverflow-1" title="" data-flip-category="">
		  		<img id="i-1" src="../img/001.png">
		  	</li>
		  	<li id="Coverflow-2" title="" data-flip-category="">
		  		<img id="i-2" src="../img/002.png">
		  	</li>
		  	<li id="Coverflow-3" title="" data-flip-category="">
		  		<img id="i-3" src="../img/003.png">
		  	</li>
		  	<li id="Coverflow-4" title="" data-flip-category="">
		  		<img id="i-4" src="../img/004.png">
		  	</li>
		  	<li id="Coverflow-5" title="" data-flip-category="">
		  		<img id="i-5" src="../img/005.png">
		  	</li>
		  	<li id="Coverflow-6" title="" data-flip-category="">
		  		<img id="i-6" src="../img/006.png">
		  	</li>
		  	<li id="Coverflow-7" title="" data-flip-category="">
		  		<img id="i-7" src="../img/007.png">
		  	</li>
		  	<li id="Coverflow-8" title="" data-flip-category="">
		  		<img id="i-8" src="../img/008.png">
		  	</li>
		  	<li id="Coverflow-9" title="" data-flip-category="">
		  		<img id="i-9" src="../img/009.png">
		  	</li>
		  	<li id="Coverflow-10" title="" data-flip-category="">
		  		<img id="i-10" src="../img/010.png">
		  	</li>
		  	<li id="Coverflow-11" title="" data-flip-category="">
		  		<img id="i-11" src="../img/011.png">
		  	</li>
		  	<li id="Coverflow-12" title="" data-flip-category="">
		  		<img id="i-12" src="../img/012.png">
		  	</li>
		  	<li id="Coverflow-13" title="" data-flip-category="">
		  		<img id="i-13" src="../img/013.png">
		  	</li>
		  	<li id="Coverflow-14" title="" data-flip-category="">
		  		<img  id="i-14" src="../img/014.png">
		  	</li>
		  	<li id="Coverflow-15" title="" data-flip-category="">
		  		<img id="i-15" src="../img/015.png">
		  	</li>
		  	<li id="Coverflow-16" title="" data-flip-category="">
		  		<img id="i-16" src="../img/016.png">
		  	</li>
		  	<li id="Coverflow-17" title="" data-flip-category="">
		  		<img id="i-17" src="../img/017.png">
		  	</li>
		  	<li id="Coverflow-18" title="" data-flip-category="">
		  		<img id="i-18" src="../img/018.png">
		  	</li>
		  	<li id="Coverflow-19" title="" data-flip-category="">
		  		<img id="i-19" src="../img/019.png">
		  	</li>
		  	<li id="Coverflow-20" title="" data-flip-category="">
		  		<img id="i-20" src="../img/020.png">
		  	</li>
		  	<li id="Coverflow-21" title="" data-flip-category="">
		  		<img id="i-21" src="../img/021.png">
		  	</li>
		  	<li id="Coverflow-22" title="" data-flip-category="">
		  		<img id="i-22" src="../img/022.png">
		  	</li>
		  </ul>
		</div>
<!-- End Flipster List -->

	<center>
		<div id="a-container" style="color: black;padding-top: 10px;">
			<div id="answers">
			</div>
			<input type="button" id="_answer" value="Answer" class="button" />
		</div>
	</center>

	</div>
</div>










<script>

	var _el = 0;
	var executed = false;
	var _questions = [];
	var _ans = [];
	var _ansr = [];


		var milisec=0;
		var seconds=0;
		var minutes=0;
		//document.d.d2.value='0'
		function display(){
			if (milisec>=9){
				milisec=0
				seconds+=1
			}
			else
				milisec+=1


			if(seconds>=60){
				seconds=0;
				minutes+=1;
			}

			//console.log(minutes+":"+seconds+":"+milisec);
			setTimeout("display()",100)
		}

    function loadData(element){

      if(_ansr.length == 22){

        var _total = 0;
        $.each(_ansr, function( index, value ) {
          if(value == true || value == "true"){
            _total+=1;
          }
        });
        window.location = "/result/index.php?k=" + _total+"&m="+minutes+"&s="+seconds+"&ms="+milisec;
      }

      if( $("#i-"+element).attr("ok") == undefined )
        $.ajax({
          url: "../functions/page.php?item="+element,
          type: "GET",
          dataType: "html",
          success: function(data){
            $(".bx-viewport").css("height","335px!important;");

            var d = JSON.parse(data);
            $("#answers").empty();
            for(var i=0; i<d.length; i++){
              $("#answers").append("<input class='_rb' type='radio' value=" + d[i].id + " id=" + d[i].id + " name='group' /> <label>" + d[i].name + "</label></br> "  );
            }
          }
        });
      else
        $.ajax({
          url: "../functions/message.php?question=q" + _el,
          type: "GET",
          dataType: "html",
          success: function(data){
            $("#answers").empty();
            var d = JSON.parse(data);

            if ($("#i-"+element).attr("ok") == "false" )
              $("#answers").append(" <div style='width=600px;height:200px;background-color:rgba(139,0,0,0.3);padding:15px 15px; '> <label style='color: white'>" + d[0].message + "</label> </div> ");
            else
              $("#answers").append(" <div style='width=600px;height:200px;background-color:rgba(34,118,0,0.3);padding:15px 15px;'> <label style='color: white'>" + d[0].message + "</label> </div> ");
          }
        });
    }



  $(function(){

    $('.bxslider').bxSlider({pager:false});

		display();

		_el =1;
		loadData(_el);

    $(".bx-next").on("click", function(){
      if(_el==22){
        _el = 1;
      }else{
        _el+=1;
      }
      loadData(_el);
    });

		$("#_answer").on("click", function(){

			var _value = $('input[name=group]:checked').val();
			_questions.push( parseInt( _el) );
			_ans.push( parseInt(_value)) ;
			$("#answers").empty();

			if(_value != undefined){
				$.ajax({
					url: "../functions/ianswer.php?answer="+ _value  + "&question=q" + _el,
				  	type: "GET",
				  	dataType: "html",
				  	success: function(data){
				  		var d = JSON.parse(data);

				  		if (d[0].result == "false"){
				  			$("#i-" + _el).attr("ok", "false");
				  			_ansr.push(false);
				  			loadData(_el);
				  		}else{
				  			$("#i-" + _el).attr("ok", "true");
				  			_ansr.push(true);
				  			loadData(_el);
				  		}

				  	}
				});
			}
		});


	});


</script>

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('5432-494-10-4139');/*]]>*/</script><noscript><a href="https://www.olark.com/site/5432-494-10-4139/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->




</body>
</html>
