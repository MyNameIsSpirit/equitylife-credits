//This js file works only to set the initial configuration of all the sliders.

var executed = false;
var content;

$(function(){

  $("#Coverflow").flipster({
  	itemContainer: 			'ul', // Container for the flippin' items.
  	itemSelector: 			'li', // Selector for children of itemContainer to flip
  	style:							'coverflow', // Switch between 'coverflow' or 'carousel' display styles
  	start: 							( window.location.hash == "" ? 0 : window.location.hash.replace("#","") ) , // Starting item. Set to 0 to start at the first, 'center' to start in the middle or the index of the item you want to start with.

  	enableKeyboard: 		true, // Enable left/right arrow navigation
  	enableMousewheel: 	false, // Enable scrollwheel navigation (up = left, down = right)
  	enableTouch: 				true, // Enable swipe navigation for touch devices

  	enableNav: 					false, // If true, flipster will insert an unordered list of the slides
  	enableNavButtons: 	true, // If true, flipster will insert Previous / Next buttons

  	onItemSwitch: 			function(){
      if(document.URL.indexOf('reports') == -1){
        var element = $(".flip-current").attr("id").split("-")[1];
        _el = element;
        loadData(element);
      }else{

      }
    }, // Callback function when items are switches
  });


});
