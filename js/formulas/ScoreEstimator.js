var Q1Val, Q2Val, Q3Val, Q4Val, Q5Val, Q6Val;



function initEstimatorSelections()
{
	alert("Im here");
		var ecdPercentCreditUsage = document.getElementById("ecdPercentCreditUsage");
		var ecdNumOfInquiries = document.getElementById("ecdNumOfInquiries");
		var ecdNumOfInstallments = document.getElementById("ecdNumOfInstallments");
		var ecdBankruptcyFiling = document.getElementById("ecdBankruptcyFiling");
		var ecdHasDelinquentAccnt = document.getElementById("ecdHasDelinquentAccnt");
		var ecdHasMortgage = document.getElementById("ecdHasMortgage");

		if (ecdPercentCreditUsage && ecdNumOfInquiries && ecdNumOfInstallments && ecdBankruptcyFiling && ecdHasDelinquentAccnt && ecdHasMortgage)
		{
			//Adjust values according to selection options/ranges
			if (Q1Val >= 0 && Q1Val <= 15)
				Q1Val = 15;
			else if (Q1Val >= 16 && Q1Val <= 29)
				Q1Val = 29;
			else if (Q1Val >=30 && Q1Val <= 50)
				Q1Val = 50;
			else if (Q1Val >=51 && Q1Val <= 64)
				Q1Val = 64;
			else if (Q1Val >=65)
				Q1Val = 65;

			if (Q2Val >= 1 && Q2Val <= 2)
				Q2Val = 2;
			else if (Q2Val >= 3 && Q2Val <= 4)
				Q2Val = 4;
			else if (Q2Val >= 5 && Q2Val <= 6)
				Q2Val = 6;
			else if (Q2Val >= 7)
				Q2Val = 7;

			if (Q3Val >= 1 && Q3Val <= 2)
				Q3Val = 2;
			else if (Q3Val >= 3 && Q3Val <= 4)
				Q3Val = 4;
			else if (Q3Val >= 5)
				Q3Val = 5;

			if (Q5Val > 0)
				Q5Val = 1;
			else
				Q5Val = 0;

			if (Q6Val > 0)
				Q6Val = 1;
			else
				Q6Val = 0;

			selectDropDownByVal(ecdPercentCreditUsage, Q1Val);
			selectDropDownByVal(ecdNumOfInquiries, Q2Val);
			selectDropDownByVal(ecdNumOfInstallments, Q3Val);
			selectDropDownByVal(ecdBankruptcyFiling, Q4Val);
			selectDropDownByVal(ecdHasDelinquentAccnt, Q5Val);
			selectDropDownByVal(ecdHasMortgage, Q6Val);
		}
}

function selectDropDownByVal(control, value)
{
	if (control)
	{
		for(i=0; i<control.length; i++)
		{
			if(control.options[i].value==value)
			{
				control.selectedIndex=i;
			}
		}
	}
}

function CalculateScore()
{
	console.log("CalculateScore");
	try
	{
		var ecdPercentCreditUsage = document.getElementById("ecdPercentCreditUsage");
		var ecdNumOfInquiries = document.getElementById("ecdNumOfInquiries");
		var ecdNumOfInstallments = document.getElementById("ecdNumOfInstallments");
		var ecdBankruptcyFiling = document.getElementById("ecdBankruptcyFiling");
		var ecdHasDelinquentAccnt = document.getElementById("ecdHasDelinquentAccnt");
		var ecdHasMortgage = document.getElementById("ecdHasMortgage");

		if (ecdPercentCreditUsage && ecdNumOfInquiries && ecdNumOfInstallments && ecdBankruptcyFiling && ecdHasDelinquentAccnt && ecdHasMortgage)
		{
			var s1 = ecdPercentCreditUsage.options[ecdPercentCreditUsage.selectedIndex].value;
			var s2 = ecdBankruptcyFiling.options[ecdBankruptcyFiling.selectedIndex].value;
			var s3 = ecdNumOfInquiries.options[ecdNumOfInquiries.selectedIndex].value;
			var s4 = ecdHasDelinquentAccnt.options[ecdHasDelinquentAccnt.selectedIndex].value;
			var s5 = ecdNumOfInstallments.options[ecdNumOfInstallments.selectedIndex].value;
			var s6 = ecdHasMortgage.options[ecdHasMortgage.selectedIndex].value;
			ECD.Web.WebControls.Controls.BEScoreCenterEstimator.CalculateScore(s1, s2, s3, s4, s5, s6, CalculateScore_CallBack);
		}
	}
	catch(e)
	{
	}
}

function CalculateScore_CallBack(response)
{
	try
	{
		if (response.error == null && response.value)
		{
			var scoreSPAN = document.getElementById("estimatedScore");
			var scoreHeader = document.getElementById("scoreHeader");
			if (scoreSPAN && scoreHeader)
			{
				scoreSPAN.firstChild.nodeValue = response.value.EstimatedScore;
				scoreHeader.firstChild.nodeValue = "Your Estimated Score is:";
			}

			var scoreDiffSPAN;
			var scoreEqual = document.getElementById("scoreEqual");
			var scoreUp = document.getElementById("scoreUp");
			var scoreDown = document.getElementById("scoreDown");
			var upImage = document.getElementById("upImage");
			var downImage = document.getElementById("downImage");
			var equalImage = document.getElementById("equalImage");

			if (response.value.ScoreDifference == 0)
			{
				scoreEqual.style.display = 'block';
				scoreUp.style.display = 'none';
				scoreDown.style.display = 'none';

				upImage.style.display = 'none';
				downImage.style.display = 'none';
				equalImage.style.display = 'block';
			}
			else if (response.value.ScoreDifference > 0)
			{
				scoreEqual.style.display = 'none';
				scoreUp.style.display = 'block';
				scoreDown.style.display = 'none';
				scoreDiffSPAN = document.getElementById("scoreDiffPos");
				scoreDiffSPAN.firstChild.nodeValue = response.value.ScoreDifference;

				upImage.style.display = 'block';
				downImage.style.display = 'none';
				equalImage.style.display = 'none';
			}
			else if (response.value.ScoreDifference < 0)
			{
				scoreEqual.style.display = 'none';
				scoreUp.style.display = 'none';
				scoreDown.style.display = 'block';
				scoreDiffSPAN = document.getElementById("scoreDiffNeg");
				scoreDiffSPAN.firstChild.nodeValue = response.value.ScoreDifference*(-1);

				upImage.style.display = 'none';
				downImage.style.display = 'block';
				equalImage.style.display = 'none';
			}
		}

	}
	catch(e) {}
}
